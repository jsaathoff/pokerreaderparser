#ifndef HHDATA_H
#define HHDATA_H

#define MAXPROVIDER 3

#include <stdio.h>
#include <iostream>
#include <ios>
#include <string>
#include <map>
#include "../../ibpp/src/ibpp/ibpp.h"
#include <boost/fusion/sequence.hpp>
#include <boost/fusion/include/sequence.hpp>
#include <boost/fusion/include/vector.hpp>
#include "standardtypes.hpp"

// using boost::wformat;
// using boost::io::group;

namespace hhdata
{

	struct SPlayerAction
	{
		SPlayerAction();
		//~SPlayerAction();

		std::wstring playername;
		int whataction;
		double amount1;
		double amount2;
		bool allin;
		int gamestate;
		datatypes::CardInfo cards;
		std::wstring finalhand;
		int pottype;
		int seat;
	};

	struct SPlayerValues
	{
		// destructor
		//~SPlayerValues();

		// constructor
		SPlayerValues();

		// database id of player
		int64_t id;

		// was initiated
		bool initiate;

		// player's seat
		int seat;

		// the players own betpot
		double currentbetpot;


		// costs per street	
		double pf_cost;
		double flop_cost;
		double turn_cost;
		double river_cost;

		// did the player mucked his cards ?
		int mucked;

		// dead small blind
		double deadsmallblind;

		// small and big blind
		double smallandbigblind;

		// dead big blind
		double deadbigblind;

		// start amount in money
		double startamount;

		// player name
		std::wstring name;

		// is it an own player ?
		int ownplayer;

		// the position
		int position;

		// facing
		int facing;

		// actioncounter...
		int actioncounter;

		// attempt to steal
		int ats;

		// volountarily put oney in pot
		int vpip;

		// raised preflop
		int pfr;

		// reaction 2 steal
		int r2steal;

		// is preflop agressor
		int pf_aggressor;

		// cold call value
		int coldcall;

		// ante posted
		double ante;

		// preflop 3bet
		int pf_3bet;

		// preflop 4bet
		int pf_4bet;

		// react to 3 bet
		int pf_r23bet;

		// react to 4bet
		int pf_r24bet;

		// preflop all-in?
		int pf_allin;

		// flop all-in?
		int flop_allin;

		// turn all-in?
		int turn_allin;

		// river all-in?
		int river_allin;

		// reaction 2 re-steal
		int r2rsteal;

		// cbet flop
		int flop_cbet;

		// cbet turn
		int turn_cbet;

		// cbet flop
		int river_cbet;

		// donkbet flop
		int flop_donkbet;

		// donkbet turn
		int turn_donkbet;

		// donkbet river
		int river_donkbet;

		// float turn
		int turn_float;

		// float river
		int river_float;

		// r2cbet on flop
		int flop_r2cbet;

		// reaction to donkbet on flop
		int flop_r2donkbet;

		// flop 4bet
		int flop_4bet;

		// flop 3bet
		int flop_3bet;

		// reaction 2 donkbet on turn
		int turn_r2donkbet;

		// reaction 2 donkbet on river
		int river_r2donkbet;

		// reaction 2 cbet on turn
		int turn_r2cbet;

		// turn 3bet
		int turn_3bet;

		// turn 4bet
		int turn_4bet;

		// river 3bet
		int river_3bet;

		// river 4bet
		int river_4bet;

		// river r2cbet
		int river_r2cbet;

		// river r2float
		int river_r2float;

		// turn r2float
		int turn_r2float;

		// preflop bets
		int pf_bets;

		// preflop calls
		int pf_calls;

		// preflop raises
		int pf_raises;

		// preflop checks
		int pf_checks;

		// preflop folds
		int pf_folds;

		// flop bets
		int flop_bets;

		// flop calls
		int flop_calls;

		// flop raises
		int flop_raises;

		// flop checks
		int flop_checks;

		// flop folds
		int flop_folds;

		// turn bets
		int turn_bets;

		// turn calls
		int turn_calls;

		// trun raises
		int turn_raises;

		// turn checks
		int turn_checks;

		// turn folds
		int turn_folds;

		// river bets
		int river_bets;

		// river calls
		int river_calls;

		// river raises
		int river_raises;

		// river checks
		int river_checks;

		// river folds
		int river_folds;

		// last action of player
		int lastaction;

		// total winning
		double winning;

		// saw flop ?
		int sawflop;

		// saw showdown
		int sawshowdown;

		// rake paid
		double rake;

		// small blind
		double smallblind;

		// big blind
		double bigblind;

		// holecards
		datatypes::CardInfo cards;

		// finalhand for player
		std::string finalhand;

		int tourneyplace;

		double tourneywinning;

		int showdowns;

		// show action on street
		int pf_show;
		int flop_show;
		int turn_show;
		int river_show;
		int sd_show;

		// pocket cards
		std::string pocketcard1;
		std::string pocketcard2;
		std::string pocketcard3;
		std::string pocketcard4;
		std::string pocketcard5;
	};

	struct SPlayerTournamentValues
	{
		// destructor
		//~SPlayerTournamentValues();

		// constructor
		SPlayerTournamentValues();

		std::wstring tournament_name;
		std::wstring playername;
		double winning;
		int bountyamount;
		double bounty;

		int place;

		double rebuy;
		int rebuyamount;

		double addon;
		int addonamount;
	};

	struct TableStructure
	{
		// constructor
		TableStructure();

		//~TableStructure();

		int64_t id;
		std::wstring name;
		int maxseats;
		int limittype;
		double limit1;
		double limit2;
		int currency;
		datatypes::DateInfo handdate;
		datatypes::TimeInfo handtime;
		int provider;
		int istourney;
		int moneytype;
	};

	struct TourneyStructure
	{
		// constructor
		TourneyStructure();

		//~TourneyStructure();

		int64_t id;

		std::wstring name;

		datatypes::DateInfo startdate;

		datatypes::DateInfo enddate;

		datatypes::TimeInfo starttime;

		datatypes::TimeInfo endtime;

		double buyin;

		double rake;

		int limittype;

		double rebuy;

		double addon;

		int playercount;

		double prizepool;

		double bounty;

		int currency;

		int provider;

		int moneytype;
	};

	struct SGameData;
	struct ProviderStructure
	{
		/*~HandContainer()
		{
		Games.erase(Games.begin(), Games.end());
		};*/

		// constructor
		ProviderStructure();

		~ProviderStructure();

		int gamecount;

		SGameData* currentgame;
		SGameData* beforegame;

		std::map<std::wstring, int64_t> GameMap;
		std::map<std::wstring, int64_t> TourneyMap;
		std::map<std::wstring, int64_t> TableMap;
		std::map<std::wstring, bool> GameAppend;
		std::map<std::wstring, bool> DuplicateGame; // container for duplicates

		std::vector<SGameData*> Games;
		std::map<std::wstring, TourneyStructure> TourneyInformation;
		std::map<std::wstring, TableStructure> Tables;
		std::map<std::wstring, int64_t> Player;
		std::map<std::wstring, int> IsOwnPlayer;

		std::wstring lasttourney;
		std::wstring lasttable;
	};

	struct HandContainer;
	struct SGameData
	{

		// constructor
		SGameData();

		// destructor
		~SGameData();

		// build player values
		void BuildPlayerValues();

		bool IsSimpleAction(int action);
		bool IsSimpleActiveAction(int action);

		// increments the player positions by decisions
		void InitPlayerPositions();

		// finally build the position
		void BuildPlayerPositions();

		// calc the facing
		void CalcFacing(int* facing, int action);

		void SetHandContainer(hhdata::HandContainer * container);

		void BuildHHText();

		void SetPlayerCount();

		void SetFileName(std::wstring filename);

		std::wstring GetFileName();

		// timestamp
		datatypes::TimeInfo timestamp;

		// date
		datatypes::DateInfo datestamp;

		std::map<std::wstring, SPlayerValues> PlayerValues;
		std::map<std::wstring, SPlayerTournamentValues> PlayerTournamentValues;
		std::map<std::wstring, bool> PlayerCounter;

		// aggressor position
		int aggressor, atspos, flop_cbet_opp, turn_cbet_opp, donkpos, float_pos, flop_donk_opp, turn_donk_opp, river_cbet_opp, river_donk_opp, turn_float_opp, river_float_opp;
		int bet3opp, bet3pos, bet4pos, bet4opp;
		// facings
		int pf_facing, flop_facing, turn_facing, river_facing, pos;
		size_t x, i;

		std::wstring m_sFilename;

		// all player actions
		std::vector<SPlayerAction> playeractions;

		// all tournament results
		std::vector<SPlayerTournamentValues> tournamentresults;

		// community cards
		datatypes::CardInfo CommunityCards;

		// set container
		hhdata::HandContainer * handcontainer;

		// temp ats opp
		int atsopp;

		// was imported
		bool imported;

		// temp position
		int tmppos;

		// is it a tournament?
		bool istournament;

		// preflop raised ?
		bool pf_raise;

		bool aggressorwasset;

		// the smallblind
		double smallblind;

		// the bigblind
		double bigblind;

		// the player count, facing, position and what was done by player
		int playercount;

		// the game duration
		double duration;

		// the game identifier
		std::wstring gamename;

		// game limit type
		int limittype;

		// gamepot total
		double totalpot;

		// sidepot
		double sidepot;

		//mainpot
		double mainpot;

		// game limit1
		double limit1;

		// game limit2
		double limit2;

		// game rake
		double gamerake;

		// gametype (real- playmoney)
		int moneytype;

		// original hand history text
		std::wstring HHTEXTORG;

		// own hh format
		std::wstring HHTEXT;

		// database id
		int64_t gamedbid;

		// time and date def
		//int hour, minute, second, day, month, year;

		// the poker provider
		int provider;

		// max. number of seats
		int maxseats;

		// name of table
		std::wstring tablename;

		// the dealer sits at seat ...
		int buttonseat;

		// is a summary
		bool issummary;

		// the currency
		int currency;

		// times a showdown happened
		int showdowncounter;

		// was appended to container
		bool isappended;

		// the tourney identifier
		std::wstring tournament_name;

		// the tourney start date
		datatypes::DateInfo tournament_startdate;

		// the tourney end date
		datatypes::DateInfo tournament_enddate;

		// the tourney start time
		datatypes::TimeInfo tournament_starttime;

		// the tourney end time
		datatypes::TimeInfo tournament_endtime;

		// the tourney identifier
		int64_t tournament_dbid;

		// the tourney buyin amount
		double tournament_buyin;

		// rebuy addon
		double tournament_rebuy;

		// prizepool
		double tournament_prizepool;

		// the bounty
		double tournament_bounty;

		// the tournament add-on
		double tournament_addon;

		// the tournament limit
		int tournament_limittype;

		// tourney rake
		double tournament_rake;

		// cap game amount
		double capamount;

		// player count int tournament
		int tournament_playercount;
	};
	struct HandContainer
	{
		// constructor
		HandContainer();

		//
		~HandContainer();

		void DeleteHands();

		void PushBackHand(hhdata::SGameData * hand);

		void CalcGames();

		std::map<int, ProviderStructure> ProviderContainer;

		int provider;

		int handcount;

		SGameData * hand;
		std::vector<SGameData *> hands;

		std::string m_hhfilename;
	};
}
#endif //HHDATA_H

/*


*/
