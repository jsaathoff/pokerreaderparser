#pragma once

#ifndef GRAMMARCONTAINER_H
#define GRAMMARCONTAINER_H

#include "textpokergrammar\textpokergrammar.hpp"

//#include "gui_con.h"

class GrammarContainer
{
public:
	GrammarContainer(void);
	~GrammarContainer(void);

	typedef std::wstring::const_iterator iterator_type;
	pokergrammar::textpokergrammar<iterator_type> gramm;

	//PokerStarsGrammarContainer * PokerStarsContainer;
	//FullTiltGrammarContainer * FullTiltContainer;
};

#endif // GRAMMARCONTAINER_H