#ifndef INCLUDES_H
#define INCLUDES_H

#define SPIRIT_ARGUMENTS_LIMIT 20
#define FUSION_MAX_VECTOR_SIZE 20

#include "../pokerreadercommon/helper/definitions.h"

//#include <boost/bind.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_statement.hpp>
#include <boost/spirit/home/phoenix/statement/if.hpp>
#include <boost/spirit/home/phoenix/statement/sequence.hpp>
//#include <boost/spirit/include/phoenix_bind.hpp>

//#include <boost/spirit/include/phoenix1_binders.hpp>
#include "hhdata.h"

BOOST_FUSION_ADAPT_STRUCT
(
	datatypes::DateInfo,
	(int, day) // 0
	(int, month) // 1
	(int, year) // 2
)

BOOST_FUSION_ADAPT_STRUCT
(
	datatypes::TimeInfo,
	(int, hour) // 0
	(int, minute) // 1
	(int, second) // 2
)

BOOST_FUSION_ADAPT_STRUCT
(
	datatypes::Cards,
	(std::wstring, card1) // 0
	(std::wstring, card2) // 1
	(std::wstring, card3) // 2
	(std::wstring, card4) // 3
	(std::wstring, card5) // 4
)

BOOST_FUSION_ADAPT_STRUCT
(
	hhdata::SPlayerAction,
	(std::wstring, playername) // 0
	(int, whataction) // 1
	(double, amount1) // 2
	(double, amount2) // 3
	(bool, allin) // 4
	(int, gamestate) // 5
	(datatypes::CardInfo, cards) // 6
	(std::wstring, finalhand) // 7
	(int, pottype) // 8
	(int, seat) // 9

	/*
	std::wstring playername;
	int whataction;
	double amount1;
	double amount2;
	bool allin;
	int gamestate;
	datatypes::Cards cards;
	std::wstring finalhand;
	int pottype;
	int seat;
	*/
)

BOOST_FUSION_ADAPT_STRUCT
(
	hhdata::SPlayerTournamentValues,
	(std::wstring, tournament_name) // 0
	(std::wstring, playername) // 1
	(double, winning) // 2
	(int, bountyamount) //3
	(double, bounty) // 4
	(int, place) // 5
	(double, rebuy) // 6
	(int, rebuyamount) // 7
	(double, addon) // 8
	(int, addonamount) // 9
)


using namespace boost::spirit::qi::standard_wide;
namespace qi = boost::spirit::qi;
using qi::symbols;
using qi::omit;
using qi::lit;
using qi::int_;
using qi::double_;
using qi::as_wstring;
using qi::locals;
using qi::rule;
using qi::_pass;
using qi::eoi;
using qi::lexeme;
using qi::no_skip;

using qi::_0;
using qi::_1;
using qi::_2;
using qi::_3;
using qi::_4;
using qi::_5;
using qi::_6;
using qi::_7;

using qi::_a;
using qi::_b;
using qi::_c;
using qi::_d;
using qi::_e;
using qi::_f;
using qi::_g;

using qi::repeat;

using qi::_val;

using qi::lazy;

//using qi::standard_wide::char_;
using qi::standard_wide::no_case;
using qi::standard_wide::alpha;
using qi::standard_wide::space;
using qi::standard_wide::space_type;

using namespace boost::phoenix;
namespace phx = boost::phoenix;
using boost::phoenix::at_c;
using boost::phoenix::if_;
using boost::phoenix::else_gen;

using boost::fusion::push_back;

#endif // INCLUDES_H
