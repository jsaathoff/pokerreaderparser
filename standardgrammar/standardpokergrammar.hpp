#ifndef STANDARDPOKERGRAMMAR_H
#define STANDARDPOKERGRAMMAR_H

#include "../includes.hpp"
#include "../standardtypes.hpp"
#include "../parsersymbols.h"
#include "../hhdata.h"
//using namespace boost::spirit::qi;


/*
	BOOST_FUSION_ADAPT_STRUCT
	(
		datatypes::DateInfo,
		(int, day) // 0
		(int, month) // 1
		(int, year) // 2
	)
	
	BOOST_FUSION_ADAPT_STRUCT
	(
		datatypes::TimeInfo,
		(int, hour) // 0
		(int, minute) // 1
		(int, second) // 2
		(std::wstring, timezone) // 3
	)
	
	BOOST_FUSION_ADAPT_STRUCT
	(
		datatypes::Cards,
		(std::wstring, card1) // 0
		(std::wstring, card2) // 1
		(std::wstring, card3) // 2
		(std::wstring, card4) // 3
		(std::wstring, card5) // 4
	)
	
	BOOST_FUSION_ADAPT_STRUCT
	(
		hhdata::SPlayerAction,
		(std::wstring, playername) // 0
		(int, whataction) // 1
		(double, amount1) // 2
		(double, amount2) // 3
		(bool, allin) // 4
		(int, gamestate) // 5
		(datatypes::CardInfo, cards) // 6
		(std::wstring, finalhand) // 7
		(int, pottype) // 8
		(int, seat) // 9*/
	

	/*
	std::wstring playername;
	int whataction;
	double amount1;
	double amount2;
	bool allin;
	int gamestate;
	datatypes::Cards cards;
	std::wstring finalhand;
	int pottype;
	int seat;
	*/
//)

namespace pokergrammar
{
	/*
	using namespace boost::spirit::qi::standard_wide;
		namespace qi = boost::spirit::qi;
		using qi::symbols;
		using qi::omit;
		using qi::lit;
		using qi::int_;
		using qi::double_;
		using qi::as_wstring;
		using qi::locals;
		using qi::rule;
		using qi::_pass;
		using qi::eoi;
		using qi::lexeme;
	
		using qi::_1;
		using qi::_2;
		using qi::_3;
		using qi::_4;
		using qi::_5;
		using qi::_6;
		using qi::_7;
	
		using qi::_a;
		using qi::_b;
		using qi::_c;
		using qi::_d;
		using qi::_e;
		using qi::_f;
		using qi::_g;
	
		using qi::repeat;
	
		using qi::_val;
	
		//using qi::standard_wide::char_;
		using qi::standard_wide::no_case;
		using qi::standard_wide::alpha;
		using qi::standard_wide::space;
		using qi::standard_wide::space_type;
	
		using namespace boost::phoenix;
		namespace phx = boost::phoenix;
		using boost::phoenix::at_c;
		using boost::phoenix::if_;
		using boost::phoenix::else_gen;
	
		using boost::fusion::push_back;
	*/
	
	template <typename Iterator>
	struct standardpokergrammar : qi::grammar<Iterator, space_type>
	{
		standardpokergrammar();


		void setContainer(hhdata::HandContainer * cont)
		{
			handcontainer = cont;
		}

		hhdata::HandContainer * handcontainer;
		hhdata::SGameData * hand;

		//std::map<int, std::wstring> infodff;

		rule<Iterator, space_type> start;
		rule<Iterator, space_type> grammarstart;

		rule<Iterator, datatypes::TimeInfo(), space_type> gettime;
		rule<Iterator, datatypes::DateInfo(), space_type> getdate;

		qi::real_parser<double, datatypes::ts_real_policies<double> > ts_real_th;
		qi::real_parser<double, qi::strict_ureal_policies<double> >const strict_double;
		qi::uint_parser<unsigned, 10, 1, 20> uint1_20;
		qi::rule<Iterator, double()> amount;

		// PLAYERACTIONS
		rule<Iterator, hhdata::SPlayerAction(), locals<std::wstring>> playerinitseataction;
		rule<Iterator, double()> playerinitseat;

		rule<Iterator, hhdata::SPlayerAction()> foldaction; // a simple action: fold, muck, check
		rule<Iterator, hhdata::SPlayerAction()> checkaction; // a simple action: fold, muck, check
		rule<Iterator, hhdata::SPlayerAction()> muckaction; // a simple action: fold, muck, check
		
		rule<Iterator, hhdata::SPlayerAction()> showaction; // a show action with card info
		rule<Iterator, hhdata::SPlayerAction()> muckwithcardsaction; // a muck with card info at summary
		rule<Iterator, hhdata::SPlayerAction()> raiseaction; // raises
		rule<Iterator, hhdata::SPlayerAction()> collectaction; // collecting a pot (collects, wins)
		rule<Iterator, hhdata::SPlayerAction()> callaction; // call a bet
		rule<Iterator, hhdata::SPlayerAction()> blindaction; // posting bigblind/smallblind/dead blind
		rule<Iterator, hhdata::SPlayerAction()> betaction; // player bets $1
		rule<Iterator, hhdata::SPlayerAction()> anteaction; // player: posts the ante 25

		rule<Iterator, hhdata::SPlayerAction()> dealtcardsaction;
		rule<Iterator, hhdata::SPlayerAction()> uncalledbetaction; // bet returned to player
		rule<Iterator> setplayeraction; // an action
		rule<Iterator, hhdata::SPlayerAction()> getallplayeractions;
		// PLAYERACTIONS

		// Get the game state
		rule<Iterator, int()> getgamestate;
		rule<Iterator> setgamestate;
		// Get the game state

		// HEADER
		rule<Iterator, void(), space_type> setcashgameheader;
		rule<Iterator, void(), space_type> settourneygameheader;
		rule<Iterator, void(), space_type> getgameheader;
		rule<Iterator, std::wstring()> gettablename;
		rule<Iterator> settablename;
		rule<Iterator, unsigned()> getmaxseats;
		rule<Iterator> setmaxseats;
		rule<Iterator, unsigned()> getbuttonseat;
		rule<Iterator> setbuttonseat;
		rule<Iterator, double()> getrake;
		rule<Iterator> setrake;


		//rule<Iterator, std::wstring()> gethandname;
		rule<Iterator, space_type> sethandname;

		//rule<Iterator, unsigned int()> gethandlimittype;
		rule<Iterator, space_type> sethandlimittype;
		rule<Iterator, space_type> settournamentlimittype;
		
		rule<Iterator, space_type> sethandlimits;
		//rule<Iterator, datatypes::TimeInfo> gethandtime;
		//rule<Iterator, datatypes::DateInfo> gethanddate;

		rule<Iterator, space_type> sethandtime;
		rule<Iterator, space_type> sethanddate;
		// HEADER

		// get alphanum string
		rule<Iterator, std::wstring()> getalphanumstring;
		// get alphanum string

		// get cards in []
		rule<Iterator, datatypes::CardInfo()> getcards;
		rule<Iterator, space_type> setboardcards;
		// get cards in []

		// get original Handhist-text
		// rule<Iterator, locals<std::wstring>> gethhtext;
		// get original Handhist-text

		// SYMBOLS
		symbols<wchar_t, unsigned> HandLimitTypes_;
		symbols<wchar_t, unsigned> TournamentLimitTypes_;
		
		symbols<wchar_t, unsigned> GameStates_;
		symbols<wchar_t, unsigned> PlayerPositions_;
		symbols<wchar_t, unsigned> CurrencyShort_;
		symbols<wchar_t, unsigned> CurrencyLong_;

		symbols<wchar_t, unsigned> FoldAction_;
		symbols<wchar_t, unsigned> MuckAction_;
		symbols<wchar_t, unsigned> CheckAction_;
		symbols<wchar_t, unsigned> CallAction_;
		symbols<wchar_t, unsigned> RaiseAction_;
		symbols<wchar_t, unsigned> ShowAction_;
		symbols<wchar_t, unsigned> CollectAction_;
		symbols<wchar_t, unsigned> UncallAction_;
		symbols<wchar_t, unsigned> BetAction_;
		symbols<wchar_t, unsigned> BlindAction_;
		symbols<wchar_t, unsigned> DealtAction_;
		symbols<wchar_t, unsigned> AnteAction_;

		
		symbols<wchar_t, bool> Seat_;
		symbols<wchar_t, bool> To_;
		symbols<wchar_t, bool> IsAllin_;
		symbols<wchar_t, bool> IsTournament_;
		symbols<wchar_t, bool> IsFreeRoll_;
		symbols<wchar_t, bool> KeyWords_;
		symbols<wchar_t, unsigned> Pot_;
		symbols<wchar_t, unsigned> SidePot_;
		symbols<wchar_t, bool> Table_;
		symbols<wchar_t, bool> ButtonSeat_;
		symbols<wchar_t, bool> IsCap_;
		//symbols<wchar_t, std::wstring> ParserSymbols::PLAYERNAMES;
		// SYMBOLS

		// local variables
		unsigned GAMESTATE;

		std::vector<std::wstring> HHTEXT; // original handhistory text

		int PROVIDER;
	};
}


#endif // STANDARDPOKERGRAMMAR_H
