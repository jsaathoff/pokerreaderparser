﻿#ifndef STANDARDPOKERGRAMMAR_DEF_H
#define STANDARDPOKERGRAMMAR_DEF_H

#include "../includes.hpp"
#include "standardpokergrammar.hpp"

namespace pokergrammar
{
	template <typename Iterator>
	standardpokergrammar<Iterator>::standardpokergrammar() : standardpokergrammar::base_type(start)
	{

		// INIT GAMESTATES
		GameStates_.add(L"flop", Definitions::GameStates::FLOP);
		//GameStates_.add(L"dealing flop", Definitions::GameStates::FLOP);
		GameStates_.add(L"turn", Definitions::GameStates::TURN);
		GameStates_.add(L"river", Definitions::GameStates::RIVER);
		GameStates_.add(L"summary", Definitions::GameStates::SUMMARY);
		GameStates_.add(L"showdown", Definitions::GameStates::SHOWDOWN);
		GameStates_.add(L"show down", Definitions::GameStates::SHOWDOWN);
		// INIT GAMESTATES

		// INIT CURRENCY
		CurrencyShort_.add(L"$", Definitions::Currency::USD);
		CurrencyShort_.add(L"€", Definitions::Currency::EUR);
		CurrencyShort_.add(L"â¬", Definitions::Currency::EUR);
		CurrencyShort_.add(L"FPP", Definitions::Currency::FPP);

		CurrencyLong_.add(L"USD", Definitions::Currency::USD);
		CurrencyLong_.add(L"EUR", Definitions::Currency::EUR);
		// INIT CURRENCY

		// INIT ACTION NAMES
		playerinitseataction.name("init playerseat-action");
		playerinitseat.name("player seat");

		foldaction.name("Action: fold"); // a simple action: fold, muck, check
		checkaction.name("Action: checks"); // a simple action: fold, muck, check
		muckaction.name("Action: muck"); // a simple action: fold, muck, check

		showaction.name("Action: show"); // a show action with card info
		muckwithcardsaction.name("Action: muckaction with cards"); // a muck with card info at summary
		raiseaction.name("Action: raise"); // raises
		collectaction.name("Action: collect"); // collecting a pot (collects, wins)
		callaction.name("Action: call"); // call a bet
		blindaction.name("Action: any blind"); // posting bigblind/smallblind/dead blind
		betaction.name("Action: bet"); // player bets $1

		dealtcardsaction.name("Action: player pocket cards");
		uncalledbetaction.name("Action: uncalled bet returns"); // bet returned to player
		setplayeraction.name("Action: allplayeractions"); // an action
		getallplayeractions.name("Playeractions");
		// INIT ACTION NAMES
		
		// INIT SEAT
		Seat_.add(L"seat", true);
		// INIT SEAT
		
		GAMESTATE = Definitions::GameStates::PREFLOP;

		getdate = 
			(
				(
					(
						int_ // the year
						>>
						lit(L"/")
						>>
						int_ // the month
						>>
						lit(L"/")
						>>
						int_ // the day
					)
					[
						at_c<0>(_val) = _3,
						at_c<1>(_val) = _2,
						at_c<2>(_val) = _1
					]
				)
				|
				(
					(
						int_ // the day
						>>
						lit(L".")
						>>
						int_ // the month
						>>
						lit(L".")
						>>
						int_ // the year
					)
					[
						at_c<0>(_val) = _1,
						at_c<1>(_val) = _2,
						at_c<2>(_val) = _3
					]
				)
			)
		;

		gettime = 
			(
				int_
				>>
				lit(L":")
				>>
				int_
				>>
				lit(L":")
				>>
				int_
			)
			[
				at_c<0>(_val) = _1,
				at_c<1>(_val) = _2,
				at_c<2>(_val) = _3
			]
		;

		amount = // parse , as thousand seperator or double-type  
			(
				strict_double 
				| 
				(
					qi::attr_cast<double>(uint1_20)
					>>
					!lit(",")
				) 
				|
				ts_real_th
			)
		; //(strict_double | (boost::spirit::attr_cast<double>(uint1_20) >> !(lit(",") >> int_) | ts_real_th));

		getalphanumstring = 
			*char_(L"a-zA-Z0-9")
		;

		start =  // standard start rule
			grammarstart // here starts the grammar for provider XXX
			>>
			eoi
		;

		getgamestate = 
			*lit(L"*")
			>>
			*omit[space]
			>>
			no_case[GameStates_]
			>>
			*omit[space]
			>>
			*lit(L"*")
		;

		getcards = 
			lit(L"[") // card opener
			>>
			(
				*omit[space]
				>>
				(
					getalphanumstring[at_c<0>(_val) = _1]
					>>
					*omit[space]
					>>
					getalphanumstring[at_c<1>(_val) = _1]
					>>
					*omit[space]
					>>
					getalphanumstring[at_c<2>(_val) = _1]
					>>
					*omit[space]
					>>
					getalphanumstring[at_c<3>(_val) = _1]
				)
				|
				(
					getalphanumstring[at_c<0>(_val) = _1]
					>>
					*omit[space]
					>>
					getalphanumstring[at_c<1>(_val) = _1]
					>>
					*omit[space]
					>>
					getalphanumstring[at_c<2>(_val) = _1]
				)
				|
				(
					getalphanumstring[at_c<0>(_val) = _1]
					>>
					*omit[space]
					>>
					getalphanumstring[at_c<1>(_val) = _1]
				)
				>>
				*omit[space]
			)
			>>
			lit(L"]") // close
			>>
			*omit[space]
			>>
			*( // min 0 times to infinity
				lit(L"[") // followed by turn or river
				>>
				*omit[space]
				>>
				getalphanumstring // one, could be turn(3 times) and river(4) after repeat-loop
				[
					phx::if_(at_c<3>(_val) == L"") // if turncard is empty
					[
						at_c<3>(_val) = _1 // add turn card
					]
					.else_
					[
						at_c<4>(_val) = _1 // add river card
					]
				]
				>>
				*omit[space]
				>>
				lit(L"]")
			)
		;
				
		//debug(dateTime);
	}


}

#endif // STANDARDPOKERGRAMMAR_DEF_H
