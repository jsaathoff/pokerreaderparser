#ifndef FULLTILTGRAMMAR_DEF_H
#define FULLTILTGRAMMAR_DEF_H

#include "../includes.hpp"
#include "fulltiltgrammar.hpp"


namespace pokergrammar
{
	template <typename Iterator>
	fulltiltgrammar<Iterator>::fulltiltgrammar() //: pokerstarsgrammar::base_type(start)
	{
		// INIT LIMITTYPES
		HandLimitTypes_.add(L"hold'em no limit", Definitions::Limits::HOLDEMNOLIMIT);
		HandLimitTypes_.add(L"hold'em limit", Definitions::Limits::HOLDEMLIMIT);
		HandLimitTypes_.add(L"hold'em pot limit", Definitions::Limits::HOLDEMPOTLIMIT);
		// INIT LIMITTYPES

		// INIT POSITIONS
		PlayerPositions_.add(L"(small blind)", Definitions::PlayerPosition::SB);
		PlayerPositions_.add(L"(big blind)", Definitions::PlayerPosition::BB);
		PlayerPositions_.add(L"(button)", Definitions::PlayerPosition::BU);
		// INIT POSITIONS

		/*// INIT ACTIONS
		Actions_.add(L"folds", Definitions::Actions::FOLD);
		Actions_.add(L"checks", Definitions::Actions::CHECK);
		Actions_.add(L"raises", Definitions::Actions::RAISE);
		Actions_.add(L"calls", Definitions::Actions::CALL);
		// INIT ACTIONS*/

		// INIT ALLIN
		IsAllin_.add(L"and is all-in", true);
		// INIT ALLIN

		// INIT TO
		To_.add(L"to", true);
		// INIT TO

		// get playername, seat and startamount
		playerinitseat =
			omit[space] // exactly one space
			>>
			lit(L"(") // opener
			>>
			-omit[no_case[CurrencyShort_]] // $ or � or ...
			>>
			amount // player start amount money
			>>
			*omit[space]
			>>
			*omit[char_]
			>>
			eoi
		;

		playerinitseataction =
			(
				omit[no_case[Seat_]] // seat, platz
				>>
				*omit[space] // spaces to skip
				>>
				int_ // seat number
				>>
				lit(L":")
				>>
				omit[space] // exactly one space
				>>
				(
					+(
						char_[_a += _1] -  // playername
						(
							(
								omit[playerinitseat]
								>>
								!omit[playerinitseat]
							)
						)
					)
					>>
					(
						(
							playerinitseat
							>>
							!omit[playerinitseat]
						)
					)
				)
			)
			[
				phx::if_(ref(GAMESTATE) == Definitions::GameStates::PREFLOP)
				[
					phx::bind(&ParserSymbols::addplayername_asstring, _a),  // add the playername to the list
					at_c<0>(_val) = _a, // playername
					at_c<1>(_val) = Definitions::Actions::PLAYERINIT, // playeraction
					at_c<8>(_val) = _1, // the seat
					at_c<2>(_val) = _3, // start amount of player
					at_c<5>(_val) = ref(this->GAMESTATE),
					_pass = true
				]
				.else_
				[
					_pass = false
				]
			]
		;

			//debug(actionnocost);
			//debug(playerinitseataction);
	}
}

#endif // FULLTILTGRAMMAR_DEF_H
