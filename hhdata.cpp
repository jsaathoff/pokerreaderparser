#include "../pokerreadercommon/helper/definitions.h"
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include "boost/date_time/local_time/local_time.hpp"
#include <boost/variant.hpp>
#include "hhdata.h"

void hhdata::SGameData::SetHandContainer(hhdata::HandContainer * container)
{
	handcontainer = container;
}

/*
// build player values
void hhdata::SGameData::BuildPlayerValues()
{
	unsigned int actx = 0, resultx = 0;

#ifdef _DEBUG
	hhdata::SPlayerValues * Playerval;
#endif

	hhdata::SPlayerAction * action;
	hhdata::SPlayerTournamentValues * tvalue;

	for(resultx = 0; resultx < this->tournamentresults.size(); resultx++)
	{
		tvalue = &tournamentresults[resultx];
		handcontainer->ProviderContainer[provider].Player[tvalue->playername] = 0;
	}

	for(actx = 0; actx < this->playeractions.size(); actx++)
	{
		action = &playeractions[actx];

		if( (action->whataction < Definitions::Actions::SIMPLEACTION) && (action->whataction > Definitions::Actions::PLAYERINIT))
		{
			// add player to playerlist/provider
			handcontainer->ProviderContainer[provider].Player[action->playername] = 0;

	#ifdef _DEBUG
			Playerval = &PlayerValues[action->playername];
	#endif
			switch(action->whataction)
			{
				

				//ProvContainer->at(2).
			}
						
			//PREFLOP
			if ( action->gamestate < Definitions::GameStates::FLOP)
			{
				/*
				if( (PlayerValues[action->playername].position == 0) && ( (action->whataction != Definitions::Actions::ANTE) && (action->whataction != Definitions::Actions::PLAYERINIT) && (action->whataction != Definitions::Actions::DEALEDCARDS) ) )
				{
					InitPlayerPosition(action);
					BuildPlayerPositions();
				}
				*/
/*
				if( ((action->whataction > Definitions::Actions::PLAYERINIT) && (action->whataction < Definitions::Actions::SIMPLEACTIONNOBLINDS)))
				{
					if(action->allin)
						PlayerValues[action->playername].pf_allin =1;

					// get action and evaluate
					switch(action->whataction)
					{
						case Definitions::Actions::CALL:
							PlayerValues[action->playername].vpip = 1;
							PlayerValues[action->playername].pf_calls++;
							break;
						
						case Definitions::Actions::RAISE:
							PlayerValues[action->playername].vpip = 1;
							PlayerValues[action->playername].pfr = 1;
							PlayerValues[action->playername].pf_raises++;
							aggressor = PlayerValues[action->playername].position;
							break;
						
						case Definitions::Actions::CHECK:
							PlayerValues[action->playername].pf_checks++;
							break;
						
						case Definitions::Actions::FOLD:
							PlayerValues[action->playername].pf_folds++;
							break;
						
						case Definitions::Actions::BET:
							PlayerValues[action->playername].vpip = 1;
							PlayerValues[action->playername].pf_bets++;
							break;
					}
					// get action and evaluate

					// is the player the preflop aggressor ?
					if((aggressorwasset == false) && (PlayerValues[action->playername].position == aggressor))
					{
						PlayerValues[action->playername].pf_aggressor = true;
						aggressorwasset = true;
						if(!action->allin)
							flop_cbet_opp = 1;
					}

					// get attempt to steal
					if( (pf_facing == 0) && ( (PlayerValues[action->playername].position <= Definitions::PlayerPosition::CO) && (PlayerValues[action->playername].position != Definitions::PlayerPosition::BB)) /* && (PlayerValues[action->playername].position > Definitions::PlayerPosition::BB)*/ /*)   // no action before and in sb, co, or bu
					/*{	
						if(IsSimpleActiveAction(action->whataction))
							PlayerValues[action->playername].ats = action->whataction;
						
						switch(action->whataction)
						{
							case Definitions::Actions::CALL:
								atsopp = 3;
								break;
							
							case Definitions::Actions::FOLD:
								atsopp = 1;
								break;

							case Definitions::Actions::RAISE:
								atsopp = 2;
								atspos = PlayerValues[action->playername].position;
								break;
							
							case Definitions::Actions::CHECK:
								atsopp = 3;
								break;
						}
					}
					// get attempt to steal

					// get coldCALL
					if( (pf_facing >= Definitions::Facing::ONERAISE) && ( PlayerValues[action->playername].lastaction <= 0) ) //Definitions::Actions::RAISEd pot, no investment/player
					{
						PlayerValues[action->playername].coldcall = action->whataction;
					}
					// get coldCALL

					// get preflop 3BET
					if( (pf_facing == Definitions::Facing::ONERAISECALLERS) || (pf_facing == Definitions::Facing::ONERAISE) )
					{
						if(IsSimpleAction(action->whataction))
							PlayerValues[action->playername].pf_3bet = action->whataction;
						

						switch(action->whataction)
						{
						case Definitions::Actions::CALL:
							bet3opp = 1;
							break;

						case Definitions::Actions::FOLD:
							bet3opp = 1;
							break;

						case Definitions::Actions::RAISE:
							bet3opp = 2;
							bet3pos = PlayerValues[action->playername].position;
							break;

						case Definitions::Actions::CHECK:
							bet3opp = 1;
							break;
						}
					}
					// get preflop 3BET

					// get r23bet
					if( (bet3opp == 2) && (PlayerValues[action->playername].pf_raises == 1) && ((pf_facing == Definitions::Facing::TWORAISES) || (pf_facing == Definitions::Facing::TWORAISESCALLERS)) )
					{
						PlayerValues[action->playername].pf_r23bet = action->whataction;	
					}
					// get r3bet
					
					// get preflop 4Definitions::Actions::BET
					if( (pf_facing == Definitions::Facing::TWORAISES) || (pf_facing == Definitions::Facing::TWORAISESCALLERS) )
					{
						PlayerValues[action->playername].pf_4bet = action->whataction;

						switch(action->whataction)
						{
						case Definitions::Actions::CALL:
							bet4opp = 1;
							break;

						case Definitions::Actions::FOLD:
							bet4opp = 1;
							break;

						case Definitions::Actions::RAISE:
							bet4opp = 2;
							bet4pos = PlayerValues[action->playername].position;
							break;

						case Definitions::Actions::CHECK:
							bet4opp = 1;
							break;
						}
					}
					// get preflop 4Definitions::Actions::BET

					// get r24bet
					if( (bet4opp == 2) && (PlayerValues[action->playername].pf_raises > 0) && (pf_facing == Definitions::Facing::THREERAISES) )
					{
						PlayerValues[action->playername].pf_r24bet = action->whataction;
					}
					// get r24bet
					/* /* get preflop 4Definitions::Actions::BET
					if( ((pf_facing == 8) || (pf_facing == 7))  && (PlayerValues[action->playername].lastaction > 0) )
					{
						PlayerValues[action->playername].id = *action->whatactionPlayerID;
						PlayerValues[action->playername].pf_4bet = action->whataction;
						
					}
					// get preflop 4Definitions::Actions::BET*/

					// get r2steal
					/*if( (atsopp == 2) && (PlayerValues[action->playername].position < atspos) && (pf_facing == Definitions::Facing::ONERAISE) )
					{
						PlayerValues[action->playername].r2steal = action->whataction;
						
					}
					// get r2steal

					// get r2rsteal
					if( (atsopp == 2) && (PlayerValues[action->playername].position == atspos) && (pf_facing >= Definitions::Facing::TWORAISES) )
					{
						PlayerValues[action->playername].r2rsteal = action->whataction;
					}
					// get r2rsteal

					// get the facing
					CalcFacing(&pf_facing, action->whataction);
				}
			}
			//PREFLOP

			//FLOP
			if (action->gamestate == Definitions::GameStates::FLOP)
			{
				if( action->whataction < Definitions::Actions::SIMPLEACTIONNOBLINDS )
				{
					if(action->allin)
						PlayerValues[action->playername].flop_allin =1;

					PlayerValues[action->playername].sawflop = 1; // saw the flop
					
					// get action and evaluate
					switch(action->whataction)
					{
						case Definitions::Actions::CALL:
							PlayerValues[action->playername].flop_calls++;
							break;
						
						case Definitions::Actions::RAISE:
							PlayerValues[action->playername].flop_raises++;
							break;
						
						case Definitions::Actions::CHECK:
							PlayerValues[action->playername].flop_checks++;
							break;
						
						case Definitions::Actions::FOLD:
							PlayerValues[action->playername].flop_folds++;
							break;
						
						case Definitions::Actions::BET:
							PlayerValues[action->playername].flop_bets++;
							break;
					}
					// get action and evaluate

					// donkbet
					if ( (flop_cbet_opp == 1) && (PlayerValues[action->playername].position != aggressor))
					{
						
						PlayerValues[action->playername].flop_donkbet = action->whataction;

						if (action->whataction == Definitions::Actions::CHECK) // not used
						{
							flop_donk_opp = 3;
						}
						if (action->whataction == Definitions::Actions::BET) // used
						{
							donkpos = PlayerValues[action->playername].position; // donk position
							flop_donk_opp = 2;
							flop_cbet_opp = 0;
						}
					}
					// donkbet

					// cbet on flop
					if ( (flop_cbet_opp == 1) && (PlayerValues[action->playername].position == aggressor) && (flop_facing == Definitions::Facing::UNOPENED) )
					{
						
						PlayerValues[action->playername].flop_cbet = action->whataction;

						if (action->whataction == Definitions::Actions::CHECK) // not used
						{
							flop_cbet_opp = 3;
						}

						if (action->whataction == Definitions::Actions::BET) // used
						{
							flop_cbet_opp = 2;
							turn_cbet_opp = 1;
						}
						
					}
					// cbet on flop
					
					// r2cbet
					if ( (flop_cbet_opp == 2) && (PlayerValues[action->playername].position != aggressor) && ((flop_facing == Definitions::Facing::ONEBETCALLERS) || (flop_facing == Definitions::Facing::ONEBET)) )
					{
						// reaction to the cbet on flop
						PlayerValues[action->playername].flop_r2cbet = action->whataction;
						
						if (action->whataction ==Definitions::Actions::CALL) //Definitions::Actions::CALL
						{
							//PlayerValues[action->playername].turn_float = 1; // float opp on turn
							turn_float_opp = 1;
							float_pos = PlayerValues[action->playername].position;
						}
						else if (action->whataction ==Definitions::Actions::RAISE) //Definitions::Actions::RAISE
						{
							//PlayerValues[action->playername].turn_float = 1; // float opp on turn
							turn_float_opp = 0;
							turn_cbet_opp = 0;
							//float_pos = action->playername;
						}
					}
					// r2cbet

					// r2donkbet
					if ( (flop_donk_opp == 2) && (donkpos != PlayerValues[action->playername].position) )
					{
						// reaction to the donkDefinitions::Actions::BET on flop
						PlayerValues[action->playername].flop_r2donkbet = action->whataction;
					}
					// r2donkbet

					// get flop 3Definitions::Actions::BET
					if(flop_facing == 6)
					{
						PlayerValues[action->playername].flop_3bet = action->whataction;
						
					}
					// get flop 3Definitions::Actions::BET

					// get flop 4Definitions::Actions::BET
					if( (flop_facing == 8) && (PlayerValues[action->playername].lastaction > 0) )
					{
						PlayerValues[action->playername].flop_4bet = action->whataction;
						
					}
					// get flop 4Definitions::Actions::BET
					
					// get the facing
					CalcFacing(&flop_facing, action->whataction);
				}
			}
			//FLOP

			//TURN
			if (action->gamestate == Definitions::GameStates::TURN)
			{
				if( action->whataction < Definitions::Actions::SIMPLEACTIONNOBLINDS )
				{

					if(action->allin)
						PlayerValues[action->playername].turn_allin =1;

					// get action and evaluate
					switch(action->whataction)
					{
						case Definitions::Actions::CALL:
							PlayerValues[action->playername].turn_calls++;
							break;
						
						case Definitions::Actions::RAISE:
							PlayerValues[action->playername].turn_raises++;
							break;
						
						case Definitions::Actions::CHECK:
							PlayerValues[action->playername].turn_checks++;
							break;
						
						case Definitions::Actions::FOLD:
							PlayerValues[action->playername].turn_folds++;
							break;
						
						case Definitions::Actions::BET:
							PlayerValues[action->playername].turn_bets++;
							break;
					}
					// get action and evaluate

					// donkbet
					if ( (turn_cbet_opp == 1) && (PlayerValues[action->playername].position != aggressor))
					{
						PlayerValues[action->playername].turn_donkbet = action->whataction;

						if (action->whataction == Definitions::Actions::CHECK) // not used
						{
							turn_donk_opp = 3;
						}
						if (action->whataction ==Definitions::Actions::BET) // used
						{
							donkpos = PlayerValues[action->playername].position; // donk position
							turn_donk_opp = 2;
							turn_cbet_opp = 0;
						}
					}
					// donkbet

					// cbet on turn
					if ( (turn_cbet_opp == 1) && (PlayerValues[action->playername].position == aggressor) && (turn_facing == Definitions::Facing::UNOPENED) )
					{
						PlayerValues[action->playername].turn_cbet = action->whataction;

						if (action->whataction == Definitions::Actions::CHECK) // not used
						{
							turn_cbet_opp = 3;
						}	

						if (action->whataction ==Definitions::Actions::BET) // used
						{
							turn_cbet_opp = 2;
							river_cbet_opp = 1;
						}

					}
					// cbet on turn

					// r2cbet
					if ( (turn_cbet_opp == 2) && (PlayerValues[action->playername].position != aggressor) && ((turn_facing == Definitions::Facing::ONERAISECALLERS) || (turn_facing == Definitions::Facing::ONERAISE)) )
					{
						// reaction to the cbet on turn
						PlayerValues[action->playername].turn_r2cbet = action->whataction;

						if (action->whataction ==Definitions::Actions::CALL) //Definitions::Actions::CALL
						{
							//PlayerValues[action->playername].turn_float = 1; // float opp on river
							river_float_opp = 1;
							float_pos = PlayerValues[action->playername].position;
						}
						else if (action->whataction ==Definitions::Actions::RAISE) //Definitions::Actions::RAISE
						{
							//PlayerValues[action->playername].turn_float = 1; // float opp on river
							river_float_opp = 0;
							river_cbet_opp = 0;
							//float_pos = action->playername;
						}
					}
					/*else if( (turn_cbet_opp == 3) && (action->playername != aggressor) )
					{
						PlayerValues[action->playername].id = *action->whatactionPlayerID;
						

						// reaction to the cbet on turn (unused)
						//PlayerValues[action->playername].turn_r2cbet = action->whataction;
						
						if (action->whataction == CHECK) //Definitions::Actions::CALL
						{
							turn_float_opp = 2;
							PlayerValues[action->playername].turn_float = 2; // float opp on turn
						}
						else if(action->whataction ==Definitions::Actions::BET)
						{
							turn_float_opp = 3;
							PlayerValues[action->playername].turn_float = 3; // float opp on turn
						}
					}*/
					// r2cbet
					/*
					// r2donkbet
					if ( (turn_donk_opp == 2) && (donkpos != PlayerValues[action->playername].position) )
					{
						// reaction to the donkDefinitions::Actions::BET on flop
						PlayerValues[action->playername].turn_r2donkbet = action->whataction;
					}
					// r2donkbet

					// get turn 3Definitions::Actions::BET
					if(turn_facing == 6)
					{
						PlayerValues[action->playername].turn_3bet = action->whataction;
						
					}
					// get turn 3Definitions::Actions::BET

					// get turn 4Definitions::Actions::BET
					if( (turn_facing == 8) && (PlayerValues[action->playername].lastaction > 0) )
					{
						PlayerValues[action->playername].turn_4bet = action->whataction;
						
					}
					// get turn 4Definitions::Actions::BET

					// get float on turn
					if( (turn_cbet_opp == 3) && (turn_facing == Definitions::Facing::UNOPENED)/*(action->playername == float_pos)*/ /*)
					/*{
						PlayerValues[action->playername].turn_float = action->whataction;
						
						if (action->whataction == Definitions::Actions::CHECK) // not used
						{
							turn_float_opp = 3;
							//river_float_opp = 0;
						}

						if (action->whataction ==Definitions::Actions::BET) // used
						{
							turn_float_opp = 2;
							//river_cbet_opp = 3;
							//river_float_opp = 1;
						}
					}
					// get float on turn

					// r2float
					if ( (turn_float_opp == 2) && (float_pos != PlayerValues[action->playername].position) )
					{
						// reaction to the donkDefinitions::Actions::BET on flop
						PlayerValues[action->playername].turn_r2float = action->whataction;
					}
					// r2float
					
					// get the facing
					CalcFacing(&turn_facing, action->whataction);
				}
			}
			//TURN

			//RIVER
			if (action->gamestate == Definitions::GameStates::RIVER)
			{
				if(action->whataction < Definitions::Actions::SIMPLEACTIONNOBLINDS)
				{

					if(action->allin)
						PlayerValues[action->playername].river_allin = 1;

					// get action and evaluate
					switch(action->whataction)
					{
						case Definitions::Actions::CALL:
							PlayerValues[action->playername].river_calls++;
							break;
						
						case Definitions::Actions::RAISE:
							PlayerValues[action->playername].river_raises++;
							break;
						
						case Definitions::Actions::CHECK:
							PlayerValues[action->playername].river_checks++;
							break;
						
						case Definitions::Actions::FOLD:
							PlayerValues[action->playername].river_folds++;
							break;
						
						case Definitions::Actions::BET:
							PlayerValues[action->playername].river_bets++;
							break;
					}
					// get action and evaluate

					// donkbet
					if ( (river_cbet_opp == 1) && (PlayerValues[action->playername].position != aggressor))
					{
						PlayerValues[action->playername].river_donkbet = action->whataction;

						if (action->whataction == Definitions::Actions::CHECK) // not used
						{
							river_donk_opp = 3;
						}
						if (action->whataction ==Definitions::Actions::BET) // used
						{
							donkpos = PlayerValues[action->playername].position; // donk position
							river_donk_opp = 2;
							river_cbet_opp = 0;
						}
					}
					// donkbet

					// cbet on river
					if ( (river_cbet_opp == 1) && (PlayerValues[action->playername].position == aggressor) && (river_facing == Definitions::Facing::UNOPENED) )
					{
						PlayerValues[action->playername].river_cbet = action->whataction;

						if (action->whataction == Definitions::Actions::CHECK) // not used
						{
							river_cbet_opp = 3;
						}

						if (action->whataction ==Definitions::Actions::BET) // used
						{
							river_cbet_opp = 2;
						}
					}
					// cbet on river

					// r2cbet
					if ( (river_cbet_opp == 2) && (PlayerValues[action->playername].position != aggressor) && ((river_facing == Definitions::Facing::ONERAISECALLERS) || (river_facing == Definitions::Facing::ONERAISE)) )
					{
						// reaction to the cbet on flop
						PlayerValues[action->playername].river_r2cbet = action->whataction;
					}
					// r2cbet

					// r2donkbet
					if ( (river_donk_opp == 2) && (donkpos != PlayerValues[action->playername].position) )
					{
						// reaction to the donkDefinitions::Actions::BET on flop
						PlayerValues[action->playername].river_r2donkbet = action->whataction;
					}
					// r2donkbet

					// get river 3Definitions::Actions::BET
					if(river_facing == Definitions::Facing::ONERAISECALLERS)
					{
						PlayerValues[action->playername].river_3bet = action->whataction;
						
					}
					// get river 3Definitions::Actions::BET

					// get river 4Definitions::Actions::BET
					if( (river_facing == Definitions::Facing::TWORAISESCALLERS) && (PlayerValues[action->playername].lastaction > 0) )
					{
						PlayerValues[action->playername].river_4bet = action->whataction;
					}
					// get turn 4Definitions::Actions::BET

					// get float on river
					if( (river_cbet_opp == 3) && (river_facing == Definitions::Facing::UNOPENED)/*(action->playername == float_pos)*/ /*)
					/*{
						PlayerValues[action->playername].river_float = action->whataction;

						if (action->whataction == Definitions::Actions::CHECK) // not used
						{
							river_float_opp = 3;
						}

						if (action->whataction == Definitions::Actions::BET) // used
						{
							river_float_opp = 2;
						}
					}
					// get float on turn

					// r2float
					if ( (river_float_opp == 2) && (float_pos != PlayerValues[action->playername].position) )
					{
						// reaction to the floatDefinitions::Actions::BET on flop
						PlayerValues[action->playername].river_r2float = action->whataction;
					}
					// r2float
					
					// get the facing
					CalcFacing(&river_facing, action->whataction);
				}
			}
			//RIVER
			else if(action->whataction ==Definitions::Actions::RAISE)
			{
				if( (action->gamestate < Definitions::GameStates::FLOP ) && ((PlayerValues[action->playername].pf_raises < 2) && (PlayerValues[action->playername].pf_calls < 1)) )
				{
					PlayerValues[action->playername].winning -= action->amount1 - (PlayerValues[action->playername].smallblind + PlayerValues[action->playername].bigblind);
				}
				else
				{
					PlayerValues[action->playername].winning -= action->amount1;
				}
			}
			else if( (action->whataction ==Definitions::Actions::BET) || (action->whataction ==Definitions::Actions::CALL))
			{
				//PlayerValues[action->playername].currentbetpot += action->amount1;
				PlayerValues[action->playername].winning -= action->amount1;
			}
			else if(action->whataction == Definitions::Actions::SMALLBLIND)
			{
				PlayerValues[action->playername].smallblind = action->amount1;
				PlayerValues[action->playername].winning -= action->amount1;
				smallblind = action->amount1;
			}
			else if(action->whataction == Definitions::Actions::BIGBLIND)
			{
				PlayerValues[action->playername].bigblind = action->amount1;
				PlayerValues[action->playername].winning -= action->amount1;
				bigblind = action->amount1;
			}
			else if(action->whataction == Definitions::Actions::SMALLANDBIGBLIND)
			{
				PlayerValues[action->playername].winning -= (smallblind + bigblind);
				PlayerValues[action->playername].bigblind = bigblind;
				PlayerValues[action->playername].smallblind = smallblind;
			}
			else if(action->whataction == Definitions::Actions::DEADBIGBLIND)
			{
				PlayerValues[action->playername].winning -= action->amount1;
				PlayerValues[action->playername].deadbigblind = bigblind;
				//PlayerValues[action->playername].smallblind = smallblind;
			}
			else if(action->whataction == Definitions::Actions::DEADSMALLBLIND)
			{
				PlayerValues[action->playername].winning -= action->amount1;
				//PlayerValues[action->playername].bigblind = bigblind;
				PlayerValues[action->playername].deadsmallblind = smallblind;
			}		
			// set last action
			if(action->whataction < Definitions::Actions::SIMPLEACTION)
			{
				PlayerValues[action->playername].lastaction = action->whataction;
			}
			/*if( (action->whataction != COLLECT) && (action->whataction != UNCALLEDBET) && (action->whataction !=Definitions::Actions::RAISE) && (action->whataction !=Definitions::Actions::BET) )
			{
				PlayerValues[action->playername].cost += action->amount1;
			}*/
		/*}
		else
		{
			if(action->whataction == Definitions::Actions::DEALEDCARDS)
			{
				PlayerValues[action->playername].cards = action->cards;
				handcontainer->ProviderContainer[provider].IsOwnPlayer[action->playername] = 1;
			}
			if( (action->whataction == Definitions::Actions::SHOWS) || (action->whataction == Definitions::Actions::MUCK) )
			{
				//PlayerValues[action->playername].showdowns++; // MOD: 12.10.2012 - count action only
				//PlayerValues[action->playername].sawflop =1; // MOD: 12.10.2012 - count action only, showdown definitions on other location
				showdowncounter++;
			}
			if(action->whataction == Definitions::Actions::TOURNEYRESULT)
			{
				PlayerValues[action->playername].tourneyplace = (int)action->amount1;
				PlayerValues[action->playername].tourneywinning = action->amount1;
			}
			if(action->whataction == Definitions::Actions::UNCALLEDBET)
			{
				//PlayerValues[action->playername].currentbetpot -= action->amount2;
				PlayerValues[action->playername].winning += action->amount1;
			}
			if(action->whataction == Definitions::Actions::ANTE)
			{
				PlayerValues[action->playername].ante += action->amount1;
				PlayerValues[action->playername].winning -= action->amount1;
			}
			// SHOWDOWN
			if (action->gamestate == Definitions::GameStates::SHOWDOWN)
			{
				PlayerValues[action->playername].sawshowdown = 1;
				PlayerValues[action->playername].sawflop = 1;
			}

			// add, sum actions
			if(action->whataction == Definitions::Actions::COLLECT)
			{
				PlayerValues[action->playername].winning += action->amount1;
				this->totalpot += action->amount1;

				// calc pots
				switch(action->pottype)
				{
				case Definitions::PotTypes::MAINPOT:
					this->mainpot += action->amount1;
					break;
				case Definitions::PotTypes::SIDEPOT:
					this->sidepot += action->amount1;
					break;
				}
			}

			if(action->whataction == Definitions::Actions::MUCK)
			{
				PlayerValues[action->playername].mucked = 1;
			}

			if(action->whataction == Definitions::Actions::PLAYERINIT)
			{
				PlayerValues[action->playername].seat = action->seat;
				handcontainer->ProviderContainer[provider].IsOwnPlayer[action->playername] = 0;
			}

			if( !action->cards.card1.empty() )
			{
				PlayerValues[action->playername].cards = action->cards;
			}
			if( !action->finalhand.empty() )
			{
				PlayerValues[action->playername].finalhand = action->finalhand;
			}
		}
	}
}*/

/*
// calc the facing
void hhdata::SGameData::CalcFacing(int* facing, int action)
{
	
	/* MOD JS
															
	FACING	
	Unopened = 0	
	1Definitions::Actions::CALLer = 1	
	2+Definitions::Actions::CALLers = 2	
	1Definitions::Actions::BET = 3	
	1Definitions::Actions::BET +Definitions::Actions::CALLer(s) = 4	
	1Definitions::Actions::RAISE	= 5
	1Definitions::Actions::RAISE +Definitions::Actions::CALLer(s)* = 6	
	2Definitions::Actions::RAISEs = 7	
	2Definitions::Actions::RAISEs +Definitions::Actions::CALLer(s)* = 8	
	3+Definitions::Actions::RAISEs = 9	
	All-In		
	
	*Call NACHDefinitions::Actions::RAISE	

	*/
	
	/*
	switch(*facing)
	{
		case Definitions::Facing::UNOPENED: // unopened
			switch(action)
			{
				case Definitions::Actions::CALL:
					*facing = Definitions::Facing::ONECALLER; // 1Definitions::Actions::CALLer
					break;
				
				case Definitions::Actions::RAISE:
					*facing = Definitions::Facing::ONERAISE; // 1Definitions::Actions::RAISE
					break;
				
				case Definitions::Actions::BET:
					*facing = Definitions::Facing::ONEBET; // 1Definitions::Actions::BET
					break;
			}
		break;

		case Definitions::Facing::ONECALLER: // 1Definitions::Actions::CALLer
			switch(action)
			{
				case Definitions::Actions::CALL:
					*facing = Definitions::Facing::TWOCALLERS; // 2+Definitions::Actions::CALLers
					break;
				
				case Definitions::Actions::RAISE:
					*facing = Definitions::Facing::ONERAISE; // 1Definitions::Actions::RAISE
					break;
				
				case Definitions::Actions::BET:
					*facing = Definitions::Facing::ONEBET; // 1Definitions::Actions::BET
					break;
			}
		break;

		case Definitions::Facing::TWOCALLERS: // 2 +Definitions::Actions::CALLers
			switch(action)
			{
				case Definitions::Actions::CALL:
					*facing = Definitions::Facing::TWOCALLERS; // 2+Definitions::Actions::CALLers
					break;
				
				case Definitions::Actions::RAISE:
					*facing = Definitions::Facing::ONERAISE; // 1Definitions::Actions::RAISE
					break;
				
				case Definitions::Actions::BET:
					*facing = Definitions::Facing::ONEBET; // 1Definitions::Actions::BET
					break;
			}
		break;

		case Definitions::Facing::ONEBET: // 1Definitions::Actions::BET
			switch(action)
			{
				case Definitions::Actions::CALL:
					*facing = Definitions::Facing::ONEBETCALLERS; // 1Definitions::Actions::BET +Definitions::Actions::CALLers
					break;
				
				case Definitions::Actions::RAISE:
					*facing = Definitions::Facing::ONERAISE; // 1Definitions::Actions::RAISE
					break;
			}
		break;

		case Definitions::Facing::ONEBETCALLERS: // 1Definitions::Actions::BET +Definitions::Actions::CALLers
			switch(action)
			{
				case Definitions::Actions::CALL:
					*facing = Definitions::Facing::ONEBETCALLERS; // 1Definitions::Actions::BET +Definitions::Actions::CALLers
					break;
				
				case Definitions::Actions::RAISE:
					*facing = Definitions::Facing::ONERAISE; // 1Definitions::Actions::RAISE
					break;
			}
		break;

		case Definitions::Facing::ONERAISE: // 1Definitions::Actions::RAISE
			switch(action)
			{
				case Definitions::Actions::CALL:
					*facing = Definitions::Facing::ONERAISECALLERS; // 1Definitions::Actions::RAISE +Definitions::Actions::CALLers
					break;
				
				case Definitions::Actions::RAISE:
					*facing = Definitions::Facing::TWORAISES; // 2Definitions::Actions::RAISEs
					break;
			}
		break;

		case Definitions::Facing::ONERAISECALLERS: // 1Definitions::Actions::RAISE +Definitions::Actions::CALLers
			switch(action)
			{
				case Definitions::Actions::CALL:
					*facing = Definitions::Facing::ONERAISECALLERS; // 1Definitions::Actions::RAISE +Definitions::Actions::CALLers
					break;
				
				case Definitions::Actions::RAISE:
					*facing = Definitions::Facing::TWORAISES; // 2Definitions::Actions::RAISEs
					break;
			}
		break;

		case Definitions::Facing::TWORAISES: // 2Definitions::Actions::RAISEs
			switch(action)
			{
				case Definitions::Actions::CALL:
					*facing = Definitions::Facing::TWORAISESCALLERS; // 2Definitions::Actions::RAISEs +Definitions::Actions::CALLers
					break;
				
				case Definitions::Actions::RAISE:
					*facing = Definitions::Facing::THREERAISES; // 2Definitions::Actions::RAISEs
					break;
			}
		break;

		/*case Definitions::Facing::TWOCALLERS: // 2Definitions::Actions::RAISEs +Definitions::Actions::CALLers
			switch(action)
			{
				case Definitions::Actions::CALL:
					*facing = Definitions::Facing::TWORAISESCALLERS; // 2Definitions::Actions::RAISEs +Definitions::Actions::CALLers
					break;
				
				case Definitions::Actions::RAISE:
					*facing = Definitions::Facing::THREERAISES; // 3Definitions::Actions::RAISEs
					break;
			}
		break;
	}
}*/
	
/*
// increments the player positions by decisions
void hhdata::SGameData::InitPlayerPositions()
{
	bool gotpockets = false; // false till the player gets the pocket cards
	
	for(i=0; i < playeractions.size(); i++) // every playeraction...
	{
		if(playeractions[i].gamestate < Definitions::GameStates::FLOP) // ... in preflop 
		{
			if(playeractions[i].whataction == Definitions::Actions::DEALEDCARDS) // 
			{
				gotpockets = true; // set to true, so we can calc position
			}
			else if(playeractions[i].whataction == Definitions::Actions::PLAYERINIT)
			{
				PlayerValues[playeractions[i].playername].seat = playeractions[i].seat; // init players seat
			}
			else if(gotpockets && (PlayerValues[playeractions[i].playername].position == 0) ) // pocket cards were dealed and the postion was initialized...
			{
				playercount++; // inc the player count
				PlayerValues[playeractions[i].playername].position = playercount; // set the position
			}
		}
	}
	BuildPlayerPositions(); // recalc position by playercount
}*/

// destructor
//hhdata::SPlayerValues::~SPlayerValues()
//{

//}

hhdata::SGameData::~SGameData()
{

}

hhdata::SGameData::SGameData()
{
	aggressor = -1;
	atspos = -1;
	flop_cbet_opp = -1;
	turn_cbet_opp = -1;
	donkpos = -1;
	float_pos = -1;
	flop_donk_opp = -1;
	turn_donk_opp = -1;
	river_cbet_opp = -1;
	river_donk_opp = -1;
	turn_float_opp = -1;
	river_float_opp = -1;
	bet3opp = -1;
	bet3pos = -1;
	bet4pos = -1;
	bet4opp = -1;
	// facings
	pf_facing = 0;
	flop_facing = 0;
	turn_facing = 0;
	river_facing = 0;
	pos = 0;
	x = 0;
	i = 0;
	capamount = 0;

	// temp ats opp
	atsopp = -1;

	// was imported
	imported = false;

	// temp position
	tmppos = -1;

	// preflop raised ?
	pf_raise = false;

	aggressorwasset = false;

	// the smallblind
	smallblind = 0;

	// the bigblind
	bigblind = 0;

	// the player count, facing, position and what was done by player
	playercount = 0;

	// the game duration
	duration = 60;

	// the game identifier
	gamename =L"";

	// game limit type
	limittype = -1;

	// gamepot total
	totalpot = 0;

	// sidepot
	sidepot = 0;

	//mainpot
	mainpot = 0;

	// game limit1
	limit1 = 0;

	// game limit2
	limit2 = 0;

	// game rake
	gamerake = 0;

	// tourney rake
	tournament_rake = 0;

	// original hand history text
	HHTEXTORG = L"";

	// database id
	gamedbid = 0;

	// timestamp
	//ibppdatetime = NULL;

	// time and date def
	//hour, minute, second, day, month, year;

	// the poker provider
	provider = 0;

	// max. number of seats
	maxseats = 0;

	// name of table
	tablename = L"";

	// the dealer sits at seat ...
	buttonseat = 0;

	// the current gamestate : PREFLOP,FLOP,TURN,RIVER,SHOWDOWN,SUMMARY
	//gamestate = 0;

	// the currency
	currency = 0;

	tournament_prizepool = 0;

	tournament_rake = 0;

	tournament_rebuy = 0;

	tournament_addon = 0;

	tournament_bounty = 0;

	tournament_limittype = -1;

	// the tourney identifier
	tournament_name = L"";

	// the tourney identifier
	tournament_dbid = 0;

	// the tourney buyin amount
	tournament_buyin = 0;

	// rebuy addon
	tournament_rebuy = 0;

	tournament_startdate = datatypes::DateInfo();

	tournament_starttime = datatypes::TimeInfo();

	tournament_endtime = datatypes::TimeInfo();

	tournament_enddate = datatypes::DateInfo();

	tournament_playercount = 0;

	datestamp = datatypes::DateInfo();

	timestamp = datatypes::TimeInfo();

	showdowncounter = 0;

	issummary = false;

	isappended = false;

	CommunityCards = datatypes::CardInfo();

	istournament = false;

	moneytype = 0;
}

hhdata::ProviderStructure::ProviderStructure()
{
	gamecount = 0;

	currentgame = NULL;
	beforegame = NULL;

	lasttourney=L"";
	lasttable=L"";
}

hhdata::ProviderStructure::~ProviderStructure()
{
	currentgame = NULL;
	beforegame = NULL;

	Games.clear();
	GameMap.clear();
	IsOwnPlayer.clear();
	Player.clear();
	TableMap.clear();
	Tables.clear();
	TourneyInformation.clear();
	TourneyMap.clear();
}

// constructor
hhdata::SPlayerValues::SPlayerValues()
{
	smallandbigblind = 0.0;

	pf_cost = 0.0;
	flop_cost = 0.0;
	turn_cost = 0.0;
	river_cost = 0.0;

	id = -1;
	currentbetpot = 0;
	ante = 0;
	seat = 0;
	startamount = 0;
	name = L"";
	position = 0;
	ats = -1;
	vpip = 0;
	pfr = 0;
	r2steal = -1;
	pf_aggressor = false;
	coldcall = -1;
	pf_3bet = -1;
	pf_4bet = -1;
	r2rsteal = -1;
	actioncounter = 0;
	flop_cbet = -1;
	turn_cbet = -1;
	river_cbet = -1;
	flop_donkbet = -1;
	turn_donkbet = -1;
	river_donkbet = -1;
	river_float = -1;
	turn_float = -1;
	flop_r2cbet = -1;
	flop_r2donkbet = -1;
	flop_3bet = -1;
	flop_4bet = -1;
	river_r2donkbet = -1;
	turn_r2donkbet = -1;
	turn_r2cbet = -1;
	turn_3bet = -1;
	turn_4bet = -1;
	river_r2cbet = -1;
	river_r2float = -1;
	river_3bet = -1;
	river_4bet = -1;
	turn_r2float = -1;
	pf_bets = 0;
	pf_calls = 0;
	pf_raises = 0;
	pf_checks = 0;
	pf_folds = 0;
	flop_bets = 0;
	flop_calls = 0;
	flop_raises = 0;
	flop_checks = 0;
	flop_folds = 0;
	turn_bets = 0;
	turn_calls = 0;
	turn_raises = 0;
	turn_checks = 0;
	turn_folds = 0;
	river_bets = 0;
	river_calls = 0;
	river_raises = 0;
	river_checks = 0;
	river_folds = 0;
	//cost = 0;
	winning = 0;
	lastaction = -1;
	sawflop = 0;
	sawshowdown = 0;
	rake = 0;
	pf_r23bet = -1;
	pf_r24bet = -1;
	smallblind = 0;
	bigblind = 0;
	ownplayer = 0;
	cards = datatypes::CardInfo();
	finalhand = "";
	mucked = 0;
	initiate = false;

	pf_allin = 0;
	flop_allin = 0;
	turn_allin = 0;
	river_allin = 0;

	pf_show = 0;
	flop_show = 0;
	turn_show = 0;
	river_show = 0;
	sd_show = 0;

	pocketcard1 = "";
	pocketcard2 = "";
	pocketcard3 = "";
	pocketcard4 = "";
	pocketcard5 = "";

	tourneyplace = 0;

	tourneywinning = 0;

	// dead small blind
	deadsmallblind = 0;

	// dead big blind
	deadbigblind = 0;

	showdowns = 0;

}

void hhdata::HandContainer::CalcGames()
{
	int x= 0;
	
	for(x=0; x < hands.size(); x++)
	{
		hand = this->hands[x];

		switch(hand->provider)
		{
			case Definitions::Providers::TOURNAMENTSUMMARYPS:
				hand->provider = Definitions::Providers::POKERSTARS;
				break;

			case Definitions::Providers::TOURNAMENTSUMMARYFT:
				hand->provider = Definitions::Providers::FULLTILT;
				break;
		}

		if(ProviderContainer[hand->provider].beforegame != hand)
		{
			ProviderContainer[hand->provider].beforegame = hand;

			if( (!hand->gamename.empty()) || (!hand->tournament_name.empty()) )
			{
				hand->SetHandContainer(this);
				//hand->InitPlayerPositions();
				//hand->BuildPlayerValues();
				hand->SetPlayerCount();
				hand->BuildHHText();

				if(hand->currency > 0)
				{
					hand->moneytype = 1;
				}
				else
					hand->moneytype = 0;

				if(hand->istournament)
				{
					ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].name = hand->tournament_name;
					ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].buyin = hand->tournament_buyin;
					ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].currency = hand->currency;
					ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].bounty = hand->tournament_bounty;
					ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].limittype = hand->tournament_limittype;
					ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].rake = hand->tournament_rake;
					ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].enddate = hand->datestamp;
					ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].endtime = hand->timestamp;
					ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].prizepool = hand->tournament_prizepool;
					ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].provider = hand->provider;
					ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].moneytype = hand->moneytype;
					ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].playercount = hand->tournament_playercount;

					if( (ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].startdate.day < 1) )
					{
						if(hand->datestamp.day > 0)
						{
							ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].startdate = hand->datestamp;
							ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].starttime = hand->timestamp;
						}
						else if(hand->tournament_startdate.day > 0)
						{
							ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].startdate = hand->tournament_startdate;
							ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].starttime = hand->tournament_starttime;

							if(hand->tournament_enddate.day > 0)
							{
								ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].enddate = hand->tournament_enddate;
								ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].endtime = hand->tournament_endtime;
							}
							else
							{
								ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].enddate = hand->tournament_startdate;
								ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].endtime = hand->tournament_starttime;
							}
						}
					}

					hand->tablename = hand->tournament_name + L" " + hand->tablename;
					//ProviderContainer[hand->provider].TourneyInformation[hand->tournament_name].name = hand->tournament_name;
				}

				ProviderContainer[hand->provider].GameMap[hand->gamename] = 0;
				ProviderContainer[hand->provider].TableMap[hand->tablename] = 0;
				ProviderContainer[hand->provider].TourneyMap[hand->tournament_name] = 0;

				ProviderContainer[hand->provider].Tables[hand->tablename].maxseats = hand->maxseats;
				ProviderContainer[hand->provider].Tables[hand->tablename].currency = hand->currency;
				ProviderContainer[hand->provider].Tables[hand->tablename].limit1 = hand->limit1;
				ProviderContainer[hand->provider].Tables[hand->tablename].limit2 = hand->limit2;
				ProviderContainer[hand->provider].Tables[hand->tablename].limittype = hand->limittype;
				ProviderContainer[hand->provider].Tables[hand->tablename].istourney = false;
				ProviderContainer[hand->provider].Tables[hand->tablename].name = hand->tablename;
				ProviderContainer[hand->provider].Tables[hand->tablename].maxseats = hand->maxseats;
				ProviderContainer[hand->provider].Tables[hand->tablename].handdate = hand->datestamp;
				ProviderContainer[hand->provider].Tables[hand->tablename].handtime = hand->timestamp;
				ProviderContainer[hand->provider].Tables[hand->tablename].moneytype = hand->moneytype;

				ProviderContainer[hand->provider].Games.push_back(hand);
				handcount++;
			}
		}
	}
}

void hhdata::HandContainer::PushBackHand(hhdata::SGameData * hand)
{
	if( (hand != NULL) || (hand > 0) )
	{
		hands.push_back(hand);
	}
}

hhdata::TourneyStructure::TourneyStructure()
{
	id = 0;
	name = L"";
	startdate = datatypes::DateInfo();
	enddate = datatypes::DateInfo();
	starttime = datatypes::TimeInfo();
	endtime = datatypes::TimeInfo();
	buyin = 0;
	rake = 0;
	limittype = -1;
	rebuy = 0;
	addon =0 ;
	playercount = 1;
	prizepool = 0;
	bounty = 0;
	currency = 0;
	provider = 0;
	moneytype = 0;
}

hhdata::TableStructure::TableStructure()
{
	id = 0;
	name = L"";
	maxseats = 0;
	limittype = -1;
	limit1 = 0;
	limit2 = 0;
	currency = 0;
	handdate = datatypes::DateInfo();
	handtime = datatypes::TimeInfo();
	provider = 0;
	istourney = 0;
	moneytype = 0;
}

hhdata::SPlayerTournamentValues::SPlayerTournamentValues()
{
	winning = 0;
	bountyamount = 0;
	bounty = 0;
	place = 0;

	rebuy = 0;
	rebuyamount = 0;

	addon = 0;
	addonamount = 0;
	playername = L"";
	tournament_name = L"";
}

hhdata::SPlayerAction::SPlayerAction()
{
	playername = L"";
	whataction = 0;
	amount1 = 0;
	amount2 = 0;
	allin = false;
	gamestate = Definitions::GameStates::PREFLOP;
	cards = datatypes::CardInfo();
	finalhand = L"";
	pottype = 0;
	seat = 0;
}


hhdata::HandContainer::HandContainer()
{
	provider = 0;
	handcount = 0;
}

hhdata::HandContainer::~HandContainer()
{
	DeleteHands();

	std::map<int, hhdata::ProviderStructure>::iterator pit;
	for(pit=ProviderContainer.begin(); pit != ProviderContainer.end(); pit++)
	{
		pit->second.beforegame = NULL;
		pit->second.currentgame = NULL;
		pit->second.GameMap.clear();
		pit->second.Games.clear();
		pit->second.Player.clear();
		pit->second.GameAppend.clear();
		pit->second.IsOwnPlayer.clear();
		pit->second.TableMap.clear();
		pit->second.Tables.clear();
		pit->second.TourneyInformation.clear();
		pit->second.TourneyMap.clear();
	}
}

void hhdata::HandContainer::DeleteHands()
{
	int x = 0;

	for(x=0; x < hands.size(); x++)
	{
		if( (hands[x] != NULL) || (hands[x] != 0))
		{
			hands[x]->handcontainer = NULL;
			delete hands[x];
			hands[x] = NULL;
		}
	}

	hands.clear();

	std::map<int, hhdata::ProviderStructure>::iterator pit;
	for(pit=ProviderContainer.begin(); pit != ProviderContainer.end(); pit++)
	{
		pit->second.Games.clear();
	}

	
}

void hhdata::SGameData::SetPlayerCount()
{
	int i = 0;
	std::wstring name;
	for(i = 0; i < playeractions.size(); i++)
	{
		if( (playeractions[i].gamestate < Definitions::GameStates::FLOP) && (playeractions[i].whataction > Definitions::Actions::PLAYERINIT))
		{
			name = playeractions[i].playername;
			if( (playeractions[i].playername != L"DEALER") && (!PlayerCounter[playeractions[i].playername]) )
			{
				handcontainer->ProviderContainer[provider].Player[name] = 0;
				PlayerCounter[playeractions[i].playername] = true;
			}
		}
	}

	this->playercount = PlayerCounter.size();
}

void hhdata::SGameData::SetFileName(std::wstring filename)
{
	m_sFilename = filename;
}

std::wstring hhdata::SGameData::GetFileName()
{
	return m_sFilename;
}

void hhdata::SGameData::BuildHHText()
{	
	std::wstring format_str = L"";
	int i = 0;
	for(i=0; i < playeractions.size(); i++)
	{
		if(i > 1)
		{
			if(playeractions[i].gamestate > playeractions[i-1].gamestate)
			{
				switch(playeractions[i].gamestate)
				{
					case Definitions::GameStates::FLOP:
						HHTEXT.append(L"***FLOP*** |NEWLINE|");
						break;

					case Definitions::GameStates::TURN:
						HHTEXT.append(L"***TURN*** |NEWLINE|");
						break;

					case Definitions::GameStates::RIVER:
						HHTEXT.append(L"***RIVER*** |NEWLINE|");
						break;

					case Definitions::GameStates::SHOWDOWN:
						HHTEXT.append(L"***SHOWDOWN*** |NEWLINE|");
						break;
				}

				HHTEXT.append(L"\n");
			}
		}
		switch(playeractions[i].whataction)
		{
			case Definitions::Actions::CHECK:
				format_str = boost::str(boost::wformat(L"%1% |CHECK| |NEWLINE|") % playeractions[i].playername);
				break;

			case Definitions::Actions::FOLD:
				format_str = boost::str(boost::wformat(L"%1% |FOLD| |NEWLINE|") % playeractions[i].playername);
				break;

			case Definitions::Actions::SITOUT:
				format_str = boost::str(boost::wformat(L"%1% |SITOUT| |NEWLINE|") % playeractions[i].playername);
				break;

			case Definitions::Actions::CALL:
				format_str = boost::str(boost::wformat(L"%1% |CALL| |AMOUNT:%2% |ALLIN:%3%| |NEWLINE|") % playeractions[i].playername % playeractions[i].amount1 % (int)playeractions[i].allin);
				break;

			case Definitions::Actions::BET:
				format_str = boost::str(boost::wformat(L"%1% |BET| |AMOUNT:%2% |ALLIN:%3%| |NEWLINE|") % playeractions[i].playername % playeractions[i].amount1 % (int)playeractions[i].allin);
				break;

			case Definitions::Actions::ANTE:
				format_str = boost::str(boost::wformat(L"%1% |ANTE| |AMOUNT:%2% |ALLIN:%3%| |NEWLINE|") % playeractions[i].playername % playeractions[i].amount1 % (int)playeractions[i].allin);
				break;

			case Definitions::Actions::RAISE:
				if(playeractions[i].amount1 <= 0)
					format_str = boost::str(boost::wformat(L"%1% |RAISE| |TO| |AMOUNT:%2% |ALLIN:%3%| |NEWLINE|") % playeractions[i].playername % playeractions[i].amount1 % (int)playeractions[i].allin);
				else
					format_str = boost::str(boost::wformat(L"%1% |RAISE| |AMOUNT:%2% |TO| |AMOUNT:%3% |ALLIN:%4%| |NEWLINE|") % playeractions[i].playername % playeractions[i].amount1 % playeractions[i].amount2 % (int)playeractions[i].allin);
				break;

			case Definitions::Actions::SMALLBLIND:
				format_str = boost::str(boost::wformat(L"%1% |SMALL| |AMOUNT:%2% |ALLIN:%3%| |NEWLINE|") % playeractions[i].playername % playeractions[i].amount1 % (int)playeractions[i].allin);
				break;

			case Definitions::Actions::BIGBLIND:
				format_str = boost::str(boost::wformat(L"%1% |BIG| |AMOUNT:%2% |ALLIN:%3%| |NEWLINE|") % playeractions[i].playername % playeractions[i].amount1 % (int)playeractions[i].allin);
				break;

			case Definitions::Actions::SMALLANDBIGBLIND:
				format_str = boost::str(boost::wformat(L"%1% |SMALLBIG| |AMOUNT:%2% |ALLIN:%3%| |NEWLINE|") % playeractions[i].playername % playeractions[i].amount1 % (int)playeractions[i].allin);
				break;

			case Definitions::Actions::COLLECT:
				format_str = boost::str(boost::wformat(L"%1% |COLLECT| |AMOUNT:%2% |POTTYPE:%3% |NEWLINE|") % playeractions[i].playername % playeractions[i].amount1 % playeractions[i].pottype);
				break;

			case Definitions::Actions::UNCALLEDBET:
				format_str = boost::str(boost::wformat(L"|UNCALL| |AMOUNT:%1% |RETURN| %2% |NEWLINE|") % playeractions[i].amount1 % playeractions[i].playername);
				break;

			case Definitions::Actions::DEADBIGBLIND:
				format_str = boost::str(boost::wformat(L"%1% |DEADBIG| |AMOUNT:%2% |ALLIN:%3%| |NEWLINE|") % playeractions[i].playername % playeractions[i].amount1 % (int)playeractions[i].allin);
				break;

			case Definitions::Actions::DEADSMALLBLIND:
				format_str = boost::str(boost::wformat(L"%1% |DEADSMALL| |AMOUNT:%2% |ALLIN:%3%| |NEWLINE|") % playeractions[i].playername % playeractions[i].amount1 % (int)playeractions[i].allin);
				break;

			case Definitions::Actions::MUCK:
				if(playeractions[i].cards.card1.empty())
					format_str = boost::str(boost::wformat(L"%1% |MUCK| |NEWLINE|") % playeractions[i].playername);
				else
					format_str = boost::str(boost::wformat(L"%1% |MUCK| %2% %3% |NEWLINE|") % playeractions[i].playername % playeractions[i].cards.card1 % playeractions[i].cards.card2);

				break;

			case Definitions::Actions::SHOWS:

				if( (!playeractions[i].finalhand.empty()) && (!playeractions[i].cards.card1.empty()))
					format_str = boost::str(boost::wformat(L"%1% |SHOW| %2% %3% |FINALHAND:%4% |NEWLINE|") % playeractions[i].playername % playeractions[i].cards.card1 % playeractions[i].cards.card2 % playeractions[i].finalhand);
				else if( (!playeractions[i].finalhand.empty()) && (playeractions[i].cards.card1.empty()))
					format_str = boost::str(boost::wformat(L"%1% |SHOW| |FINALHAND:%2% |NEWLINE|") % playeractions[i].playername % playeractions[i].finalhand);
				else
					format_str = boost::str(boost::wformat(L"%1% |SHOW| %2% %3% |NEWLINE|") % playeractions[i].playername % playeractions[i].cards.card1 % playeractions[i].cards.card2);

				break;
		}

		if(!format_str.empty())
		{
			HHTEXT.append(format_str);
			HHTEXT.append(L"\n");
		}

		format_str = L"";
	}
}

void hhdata::SGameData::BuildPlayerPositions()
{
	int newposition = 0;
	std::map<std::wstring, hhdata::SPlayerValues>::iterator it;
	std::map<int,int> Positions;

	switch(playercount)
	{
		case 10:
			Positions[8] = Definitions::PlayerPosition::BU;
			Positions[7] = Definitions::PlayerPosition::CO;
			Positions[6] = Definitions::PlayerPosition::MP3;
			Positions[5] = Definitions::PlayerPosition::MP2;
			Positions[4] = Definitions::PlayerPosition::MP1;
			Positions[3] = Definitions::PlayerPosition::EP3;
			Positions[2] = Definitions::PlayerPosition::EP2;
			Positions[1] = Definitions::PlayerPosition::EP1;
			
			Positions[9] = Definitions::PlayerPosition::SB;
			Positions[10] = Definitions::PlayerPosition::BB;
			break;

		case 9:
			Positions[7] = Definitions::PlayerPosition::BU;
			Positions[6] = Definitions::PlayerPosition::CO;
			Positions[5] = Definitions::PlayerPosition::MP3;
			Positions[4] = Definitions::PlayerPosition::MP2;
			Positions[3] = Definitions::PlayerPosition::MP1;
			Positions[2] = Definitions::PlayerPosition::EP2;
			Positions[1] = Definitions::PlayerPosition::EP1;
			
			Positions[8] = Definitions::PlayerPosition::SB;
			Positions[9] = Definitions::PlayerPosition::BB;
			break;

		case 8:
			Positions[6] = Definitions::PlayerPosition::BU;
			Positions[5] = Definitions::PlayerPosition::CO;
			Positions[4] = Definitions::PlayerPosition::MP2;
			Positions[3] = Definitions::PlayerPosition::MP1;
			Positions[2] = Definitions::PlayerPosition::EP2;
			Positions[1] = Definitions::PlayerPosition::EP1;
			
			Positions[7] = Definitions::PlayerPosition::SB;
			Positions[8] = Definitions::PlayerPosition::BB;
			break;

		case 7:
			Positions[5] = Definitions::PlayerPosition::BU;
			Positions[4] = Definitions::PlayerPosition::CO;
			Positions[3] = Definitions::PlayerPosition::MP2;
			Positions[2] = Definitions::PlayerPosition::MP1;
			Positions[1] = Definitions::PlayerPosition::EP1;
			
			Positions[6] = Definitions::PlayerPosition::SB;
			Positions[7] = Definitions::PlayerPosition::BB;
			break;

		case 6:
			Positions[4] = Definitions::PlayerPosition::BU;
			Positions[3] = Definitions::PlayerPosition::CO;
			Positions[2] = Definitions::PlayerPosition::MP1;
			Positions[1] = Definitions::PlayerPosition::EP1;
			
			Positions[5] = Definitions::PlayerPosition::SB;
			Positions[6] = Definitions::PlayerPosition::BB;
			break;

		case 5:
			Positions[3] = Definitions::PlayerPosition::BU;
			Positions[2] = Definitions::PlayerPosition::CO;
			Positions[1] = Definitions::PlayerPosition::EP1;
			
			Positions[4] = Definitions::PlayerPosition::SB;
			Positions[5] = Definitions::PlayerPosition::BB;
			break;

		case 4:
			Positions[1] = Definitions::PlayerPosition::BU;
			Positions[2] = Definitions::PlayerPosition::CO;
			
			Positions[3] = Definitions::PlayerPosition::SB;
			Positions[4] = Definitions::PlayerPosition::BB;
			break;

		case 3:
			Positions[1] = Definitions::PlayerPosition::BU;
			
			Positions[2] = Definitions::PlayerPosition::SB;
			Positions[3] = Definitions::PlayerPosition::BB;
			break;

		case 2:
			Positions[1] = Definitions::PlayerPosition::SB;
			Positions[2] = Definitions::PlayerPosition::BB;
			break;
	}

	for(it = PlayerValues.begin(); it != PlayerValues.end(); it++)
	{
		it->second.position = Positions[it->second.position];
	}
}

bool hhdata::SGameData::IsSimpleActiveAction(int action)
{
	switch(action)
	{
		case Definitions::Actions::CALL:
		case Definitions::Actions::CHECK:
		case Definitions::Actions::BET:
		case Definitions::Actions::RAISE:
		case Definitions::Actions::FOLD:
			return true;
			break;
		default:
			return false;
			break;
	}
}

bool hhdata::SGameData::IsSimpleAction(int action)
{
	switch(action)
	{
	case Definitions::Actions::CALL:
	case Definitions::Actions::CHECK:
	case Definitions::Actions::BET:
	case Definitions::Actions::RAISE:
	case Definitions::Actions::SMALLBLIND:
	case Definitions::Actions::BIGBLIND:
	case Definitions::Actions::DEADBIGBLIND:
	case Definitions::Actions::DEADSMALLBLIND:
	case Definitions::Actions::FOLD:
	case Definitions::Actions::SMALLANDBIGBLIND:
		return true;
		break;
	default:
		return false;
		break;
	}
}