/////////////////////////////////////////////////////////////////////////////
// Name:        pokerreaderparserlib2guiapp.cpp
// Purpose:     
// Author:      
// Modified by: 
// Created:     02/09/2011 11:48:04
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

////@begin includes
////@end includes

#include "pokerreaderparserlib2guiapp.h"

////@begin XPM images
////@end XPM images

#include "gui_con.h"


/*
 * Application instance implementation
 */

////@begin implement app
IMPLEMENT_APP( PokerReaderParserLib2GuiApp )
////@end implement app


/*
 * PokerReaderParserLib2GuiApp type definition
 */

IMPLEMENT_CLASS( PokerReaderParserLib2GuiApp, wxApp )


/*
 * PokerReaderParserLib2GuiApp event table definition
 */

BEGIN_EVENT_TABLE( PokerReaderParserLib2GuiApp, wxApp )

////@begin PokerReaderParserLib2GuiApp event table entries
////@end PokerReaderParserLib2GuiApp event table entries

END_EVENT_TABLE()


/*
 * Constructor for PokerReaderParserLib2GuiApp
 */

PokerReaderParserLib2GuiApp::PokerReaderParserLib2GuiApp()
{
    Init();
}


/*
 * Member initialisation
 */

void PokerReaderParserLib2GuiApp::Init()
{
////@begin PokerReaderParserLib2GuiApp member initialisation
////@end PokerReaderParserLib2GuiApp member initialisation
}

/*
 * Initialisation for PokerReaderParserLib2GuiApp
 */

bool PokerReaderParserLib2GuiApp::OnInit()
{    
////@begin PokerReaderParserLib2GuiApp initialisation
	// Remove the comment markers above and below this block
	// to make permanent changes to the code.

#if wxUSE_XPM
	wxImage::AddHandler(new wxXPMHandler);
#endif
#if wxUSE_LIBPNG
	wxImage::AddHandler(new wxPNGHandler);
#endif
#if wxUSE_LIBJPEG
	wxImage::AddHandler(new wxJPEGHandler);
#endif
#if wxUSE_GIF
	wxImage::AddHandler(new wxGIFHandler);
#endif
	MainFrame* mainWindow = new MainFrame( NULL );
	mainWindow->Show(true);
////@end PokerReaderParserLib2GuiApp initialisation
	//RedirectIOToConsole();
    return true;
}


/*
 * Cleanup for PokerReaderParserLib2GuiApp
 */

int PokerReaderParserLib2GuiApp::OnExit()
{    
////@begin PokerReaderParserLib2GuiApp cleanup
	return wxApp::OnExit();
////@end PokerReaderParserLib2GuiApp cleanup
}

