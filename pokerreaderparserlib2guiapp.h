/////////////////////////////////////////////////////////////////////////////
// Name:        pokerreaderparserlib2guiapp.h
// Purpose:     
// Author:      
// Modified by: 
// Created:     02/09/2011 11:48:04
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _POKERREADERPARSERLIB2GUIAPP_H_
#define _POKERREADERPARSERLIB2GUIAPP_H_


/*!
 * Includes
 */

////@begin includes
#include "wx/image.h"
#include "mainframe.h"
////@end includes

/*!
 * Forward declarations
 */

////@begin forward declarations
////@end forward declarations

/*!
 * Control identifiers
 */

////@begin control identifiers
////@end control identifiers

/*!
 * PokerReaderParserLib2GuiApp class declaration
 */

class PokerReaderParserLib2GuiApp: public wxApp
{    
    DECLARE_CLASS( PokerReaderParserLib2GuiApp )
    DECLARE_EVENT_TABLE()

public:
    /// Constructor
    PokerReaderParserLib2GuiApp();

    void Init();

    /// Initialises the application
    virtual bool OnInit();

    /// Called on exit
    virtual int OnExit();

////@begin PokerReaderParserLib2GuiApp event handler declarations

////@end PokerReaderParserLib2GuiApp event handler declarations

////@begin PokerReaderParserLib2GuiApp member function declarations

////@end PokerReaderParserLib2GuiApp member function declarations

////@begin PokerReaderParserLib2GuiApp member variables
////@end PokerReaderParserLib2GuiApp member variables
};

/*!
 * Application instance declaration 
 */

////@begin declare app
DECLARE_APP(PokerReaderParserLib2GuiApp)
////@end declare app

#endif
    // _POKERREADERPARSERLIB2GUIAPP_H_
