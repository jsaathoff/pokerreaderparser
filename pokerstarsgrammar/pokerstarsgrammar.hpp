#ifndef POKERSTARSGRAMMAR_H
#define POKERSTARSGRAMMAR_H

#include "../includes.hpp"
#include "../standardtypes.hpp"
#include "../standardgrammar/standardpokergrammar.hpp"

namespace pokergrammar
{
	template <typename Iterator>
	struct pokerstarsgrammar : pokergrammar::standardpokergrammar<Iterator>
	{
		pokerstarsgrammar();

		rule<Iterator, hhdata::SPlayerTournamentValues()> tournamentresultaction;
		rule<Iterator, double()> tournamentwinamount;
		rule<Iterator, hhdata::SPlayerTournamentValues()> bountyaction;
		rule<Iterator, hhdata::SPlayerTournamentValues()> rebuyaction;
		rule<Iterator> settournamentresult;

		rule<Iterator, locals<hhdata::SPlayerAction> > setdealeraction;

		symbols<wchar_t, bool> BountyAction_;
		symbols<wchar_t, bool> ReceiveAction_;
		symbols<wchar_t, bool> RebuyAction_;
	};
}


#endif // POKERSTARSGRAMMAR_H
