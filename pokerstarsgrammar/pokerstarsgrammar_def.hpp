#ifndef POKERSTARSGRAMMAR_DEF_H
#define POKERSTARSGRAMMAR_DEF_H

#include "../includes.hpp"
#include "pokerstarsgrammar.hpp"


namespace pokergrammar
{
	template <typename Iterator>
	pokerstarsgrammar<Iterator>::pokerstarsgrammar() //: pokerstarsgrammar::base_type(start)
	{
		// INIT LIMITTYPES
		HandLimitTypes_.add(L"hold'em no limit", Definitions::Limits::HOLDEMNOLIMIT);
		HandLimitTypes_.add(L"hold'em limit", Definitions::Limits::HOLDEMLIMIT);
		HandLimitTypes_.add(L"hold'em pot limit", Definitions::Limits::HOLDEMPOTLIMIT);

		TournamentLimitTypes_.add(L"hold'em mixed", Definitions::Limits::HOLDEMMIXED);
		// INIT LIMITTYPES

		// INIT POSITIONS
		PlayerPositions_.add(L"(small blind)", Definitions::PlayerPosition::SB);
		PlayerPositions_.add(L"(big blind)", Definitions::PlayerPosition::BB);
		PlayerPositions_.add(L"(button)", Definitions::PlayerPosition::BU);
		// INIT POSITIONS

		// init table prefix
		Table_.add(L"table", true);
		// init table prefix

		// init button
		ButtonSeat_.add(L"is the button", true);
		// init button

		// INIT ACTIONS
		FoldAction_.add(L"folds", Definitions::Actions::FOLD);
		//FoldAction_.add(L"passt", Definitions::Actions::FOLD);
		CheckAction_.add(L"checks", Definitions::Actions::CHECK);
		RaiseAction_.add(L"raises", Definitions::Actions::RAISE);
		CallAction_.add(L"calls", Definitions::Actions::CALL);
		ShowAction_.add(L"shows", Definitions::Actions::SHOWS);
		BetAction_.add(L"bets", Definitions::Actions::BET);
		BlindAction_.add(L"posts small blind", Definitions::Actions::SMALLBLIND);
		BlindAction_.add(L"posts big blind", Definitions::Actions::BIGBLIND);
		BlindAction_.add(L"posts a dead small blind of", Definitions::Actions::DEADSMALLBLIND);
		BlindAction_.add(L"posts a dead big blind of", Definitions::Actions::DEADBIGBLIND);
		BlindAction_.add(L"posts small & big blinds", Definitions::Actions::SMALLANDBIGBLIND);
		UncallAction_.add(L"uncalled bet", Definitions::Actions::UNCALLEDBET);
		DealtAction_.add(L"dealt to", Definitions::Actions::DEALEDCARDS);
		MuckAction_.add(L"mucks hand", Definitions::Actions::MUCK);
		MuckAction_.add(L"mucked", Definitions::Actions::MUCK);
		//MuckAction_.add(L"doesn't show hand", Definitions::Actions::MUCK);
		CollectAction_.add(L"collected", Definitions::Actions::COLLECT);
		AnteAction_.add(L"posts the ante", Definitions::Actions::ANTE);

		ReceiveAction_.add(L"finished the tournament in", true);
		//ReceiveAction_.add(L"place", true);
		//ReceiveAction_.add(L"and received", true);
		//ReceiveAction_.add(L"bounty", true);
		ReceiveAction_.add(L"wins the tournament and receives", true);

		RebuyAction_.add(L"re-buys and receives", true);
		// INIT ACTIONS

		// INIT
		IsTournament_.add(L"tournament", true);
		// INIT

		// INIT KEYWORDS
		KeyWords_.add(L"returned to", true);
		KeyWords_.add(L"from", true);
		KeyWords_.add(L"congratulations!", true);
		// INIT KEYWORDS

		// INIT POT
		Pot_.add(L"main pot", Definitions::PotTypes::MAINPOT);
		Pot_.add(L"pot", Definitions::PotTypes::MAINPOT);
		SidePot_.add(L"side pot", Definitions::Actions::COLLECTSIDEPOT1);
		SidePot_.add(L"side pot-1", Definitions::Actions::COLLECTSIDEPOT1);
		SidePot_.add(L"side pot-2", Definitions::Actions::COLLECTSIDEPOT2);
		SidePot_.add(L"side pot-3", Definitions::Actions::COLLECTSIDEPOT3);
		SidePot_.add(L"side pot-4", Definitions::Actions::COLLECTSIDEPOT4);
		SidePot_.add(L"side pot-5", Definitions::Actions::COLLECTSIDEPOT5);
		SidePot_.add(L"side pot-6", Definitions::Actions::COLLECTSIDEPOT6);
		SidePot_.add(L"side pot-7", Definitions::Actions::COLLECTSIDEPOT7);
		SidePot_.add(L"side pot-8", Definitions::Actions::COLLECTSIDEPOT8);
		SidePot_.add(L"side pot-9", Definitions::Actions::COLLECTSIDEPOT9);
		// INIT POT

		// INIT ALLIN
		IsAllin_.add(L"and is all-in", true);
		// INIT ALLIN

		// INIT TO
		To_.add(L"to", true);
		// INIT TO

		// INIT FREEROLL
		IsFreeRoll_.add(L"freeroll", true);
		// INIT FREEROLL

		// INIT CAP
		IsCap_.add(L"cap", true);
		// INIT CAP

		// INIT Bounty
		BountyAction_.add(L"wins the", true);
		// INIT Bounty

		// get playername, seat and startamount
		playerinitseat.name("player init... the end");
		playerinitseat =
			omit[space] // exactly one space
			>>
			lit(L"(") // opener
			>>
			-omit[CurrencyShort_] // $ or � or ...
			>>
			amount // player start amount money
			>>
			*omit[space]
			>>
			*omit[char_]
			>>
			eoi
		;

		// player seat initializ
		playerinitseataction.name("playerinit complete");
		playerinitseataction =
			(
				omit[no_case[Seat_]] // seat, platz
				>>
				+omit[space] // spaces to skip
				>>
				int_ // seat number
				>>
				lit(L":")
				>>
				omit[space] // exactly one space
				>>
				+(
					lexeme[char_[_a += _1]] -  // playername
					(
						(
							omit[playerinitseat]
							>>
							!omit[playerinitseat]
						)
					)
				)
				>>
				(
					(
						playerinitseat
						>>
						!omit[playerinitseat]
					)
				)
			)
			[
				phx::if_(ref(GAMESTATE) == Definitions::GameStates::PREFLOP)
				[
					phx::bind(&ParserSymbols::addplayername_asstring, _a),  // add the playername to the list
					at_c<0>(_val) = _a, // playername
					at_c<1>(_val) = Definitions::Actions::PLAYERINIT, // playeraction
					at_c<9>(_val) = _1, //, // the seat
					at_c<2>(_val) = _3, //, // start amount of player
					at_c<5>(_val) = ref(this->GAMESTATE), // set current gamestate in action
					_pass = true
				]
				.else_
				[
					_pass = false
				]
			]
		;

		// a player folds
		foldaction.name("get a fold action");
		foldaction =
			ParserSymbols::PLAYERNAMES
			[
				at_c<0>(_val) = _1 ,
				at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			lit(L":")
			>>
			*omit[space] // one or more spaces
			>>
			no_case[FoldAction_][at_c<1>(_val) = _1] // the action: fold
			>>
			-omit[space] // MOD: 11.10.2012
			>> // ADDED : 11.10.2012 Holecards after action
			*omit[char_] // [7h ]
			>>
			eoi
		;

		// a player mucks cards
		muckaction.name("muckaction");
		muckaction =
			ParserSymbols::PLAYERNAMES
			[
				at_c<0>(_val) = _1 ,
				at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			lit(L":")
			>>
			*omit[space] // one or more spaces
			>>
			no_case[MuckAction_][at_c<1>(_val) = _1] // the action: mucks
			>>
			*omit[space]
			>>
			eoi
		;

		// the player checks
		checkaction.name("check action");
		checkaction =
			ParserSymbols::PLAYERNAMES
			[
				at_c<0>(_val) = _1 ,
				at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			lit(L":")
			>>
			*omit[space] // one or more spaces
			>>
			no_case[CheckAction_][at_c<1>(_val) = _1] // the action: checks
			>>
			*omit[space]
			>>
			eoi
		;

		// the player calls
		callaction.name("call action");
		callaction =
			ParserSymbols::PLAYERNAMES
			[
				at_c<0>(_val) = _1 ,
				at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			lit(L":")
			>>
			*omit[space] // one or more spaces
			>>
			no_case[CallAction_][at_c<1>(_val) = _1] // the action: call
			>>
			*omit[space]
			>>
			-omit[no_case[CurrencyShort_]] // short term for currency, not in player action
			>>
			amount[at_c<2>(_val) = _1]
			>>
			*omit[space]
			>>
			-no_case[IsAllin_][at_c<4>(_val) = _1]
			>>
			*omit[space]
			>>
			eoi
		;

		// get a bet action
		betaction.name("bet action");
		betaction =
			ParserSymbols::PLAYERNAMES
			[
				at_c<0>(_val) = _1 ,
				at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			lit(L":")
			>>
			*omit[space] // one or more spaces
			>>
			no_case[BetAction_][at_c<1>(_val) = _1] // the action: bet
			>>
			*omit[space]
			>>
			-omit[no_case[CurrencyShort_]] // short term for currency, not in player action
			>>
			amount[at_c<2>(_val) = _1]
			>>
			*omit[space]
			>>
			-no_case[IsAllin_][at_c<4>(_val) = _1]
			>>
			*omit[space]
			>>
			eoi
		;

		// get a blind action
		blindaction.name("blind action");
		blindaction =
			ParserSymbols::PLAYERNAMES
			[
				at_c<0>(_val) = _1 ,
				at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			lit(L":")
			>>
			*omit[space] // one or more spaces
			>>
			no_case[BlindAction_][at_c<1>(_val) = _1] // the action: bet
			>>
			*omit[space]
			>>
			-omit[no_case[CurrencyShort_]] // short term for currency, not in player action
			>>
			amount[at_c<2>(_val) = _1]
			>>
			*omit[space]
			>>
			-no_case[IsAllin_][at_c<4>(_val) = _1]
			>>
			*omit[space]
			>>
			eoi
		;
		
			// get an ante action
		anteaction.name("ante action");
		anteaction =
			ParserSymbols::PLAYERNAMES
			[
				at_c<0>(_val) = _1 ,
				at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			lit(L":")
			>>
			*omit[space] // one or more spaces
			>>
			no_case[AnteAction_][at_c<1>(_val) = _1] // the action: ante
			>>
			*omit[space]
			>>
			-omit[no_case[CurrencyShort_]] // short term for currency, not in player action
			>>
			amount[at_c<2>(_val) = _1]
			>>
			*omit[space]
			>>
			-no_case[IsAllin_][at_c<4>(_val) = _1]
			>>
			*omit[space]
			>>
			eoi
		;

		// get a raise action
		raiseaction.name("raise action");
		raiseaction =
			ParserSymbols::PLAYERNAMES
			[
				at_c<0>(_val) = _1 ,
				at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			lit(L":")
			>>
			omit[space] // one or more spaces
			>>
			no_case[RaiseAction_][at_c<1>(_val) = _1] // the action: bet
			>>
			*omit[space]
			>>
			-omit[no_case[CurrencyShort_]] // short term for currency, not in player action
			>>
			amount[at_c<2>(_val) = _1]
			>>
			*omit[space]
			>>
			omit[To_]
			>>
			*omit[space]
			>>
			-omit[no_case[CurrencyShort_]] // short term for currency, not in player action
			>>
			amount[at_c<3>(_val) = _1]
			>>
			*omit[space]
			>>
			-no_case[IsAllin_][at_c<4>(_val) = _1]
			>>
			*omit[space]
			>>
			eoi
		;

		// get an uncall action
		uncalledbetaction.name("uncalled bet");
		uncalledbetaction =
			no_case[UncallAction_][at_c<1>(_val) = _1]
			>>
			*omit[space]
			>>
			lit(L"(")
			>>
			-omit[CurrencyShort_]
			>>
			amount[at_c<2>(_val) = _1]
			>>
			lit(L")")
			>>
			*omit[space]
			>>
			omit[KeyWords_]
			>>
			*omit[space]
			>>
			ParserSymbols::PLAYERNAMES
			[
				at_c<0>(_val) = _1 ,
				at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			*omit[space]
			>>
			eoi
		;

		// get a dealt action
		dealtcardsaction.name("dealt cards to player");
		dealtcardsaction =
			no_case[DealtAction_][at_c<1>(_val) = _1]
			>>
			omit[space]
			>>
			ParserSymbols::PLAYERNAMES
			[
				at_c<0>(_val) = _1 ,
				at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			*omit[space]
			>>
			getcards[at_c<6>(_val) = _1]
			>>
			*omit[space]
			>>
			eoi
		;

		// get a show action
		showaction.name("show action");
		showaction =
			ParserSymbols::PLAYERNAMES
			[
				at_c<0>(_val) = _1 ,
				at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			lit(L":")
			>>
			*omit[space]
			>>
			no_case[ShowAction_][at_c<1>(_val) = _1]
			>>
			*omit[space]
			>>
			getcards[at_c<6>(_val) = _1]
			>>
			*omit[space]
			>>
			-(
				lit(L"(")
				>>
				qi::as_wstring[+~char_(L")")][at_c<7>(_val) = _1]
				>>
				lit(L")")
			)
			>>
			*omit[space]
			>>
			eoi
		;

		// mucked card pair
		muckwithcardsaction.name("get a card pair mucked");
		muckwithcardsaction =
			omit[no_case[Seat_]]
			>>
			*omit[space]
			>>
			omit[int_]
			>>
			lit(L":")
			>>
			omit[space]
			>>
			ParserSymbols::PLAYERNAMES
			[
				at_c<0>(_val) = _1 ,
				at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			*omit[space]
			>>
			-(
				omit[PlayerPositions_]
				>>
				omit[space]
			)
			>>
			MuckAction_[at_c<1>(_val) = _1]
			>>
			*omit[space]
			>>
			getcards[at_c<6>(_val) = _1]
			>>
			*omit[space]
			>>
			eoi
		;

		// the player wins a pot
		collectaction.name("get a collect action");
		collectaction =
			ParserSymbols::PLAYERNAMES
			[
				at_c<0>(_val) = _1 ,
				at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			*omit[space]
			>>
			no_case[CollectAction_][at_c<1>(_val) = _1]
			>>
			*omit[space]
			>>
			-omit[no_case[CurrencyShort_]]
			>>
			amount[at_c<2>(_val) = _1]
			>>
			*omit[space]
			>>
			omit[no_case[KeyWords_]]
			>>
			*omit[space]
			>>
			(
				no_case[Pot_][at_c<8>(_val) = _1]
				|
				no_case[SidePot_][at_c<1>(_val) = _1]
			)
			>>
			*omit[char_]
			>>
			eoi
			//-omit[int_]
			//>>
			//eoi
		;

		// sets the whole header for a cashgame
		setcashgameheader.name("get cashgame header");
		setcashgameheader =
			sethandlimittype // get the the limittype (holdem ... no limit, pot limit, limit)
			>>
			*omit[space]
			>>
			lit(L"(") //
			>>
			-CurrencyShort_ // short term for currency
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::currency, phx::ref(hand)) = _1 // set currency of hand
				]
			]
			>>
			amount
			[
				phx::if_( phx::ref(hand) )
				[
					std::wcout << L"Limit1 " << _1,
					phx::bind(&hhdata::SGameData::limit1, phx::ref(hand)) = _1 // set limit1
				]
			]
			>>
			*omit[space]
			>>
			lit(L"/") // delimit
			>>
			*omit[space]
			>>
			-omit[CurrencyShort_] // omit the next currency
			>>
			amount
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::limit2, phx::ref(hand)) = _1 // set limit2
				]
			]
			>>
			*omit[space]
			>>
			-( // get the cap, exactly one times or not present in hand
				lit(L"-") // delimiter, exactly one
				>>
				*omit[space] // no, one or more spaces
				>>
				-omit[CurrencyShort_] // $ �
				>>
				amount
				[
					phx::if_( phx::ref(hand) )
					[
						phx::bind(&hhdata::SGameData::capamount, phx::ref(hand)) = _1 // set cap amount
					]
				]
				>>
				*omit[space] // no, one or more spaces
				>>
				omit[no_case[IsCap_]] // the word cap
				>>
				*omit[space] // no, one or more spaces
				>>
				lit(L"-") // delimiter
				>>
				*omit[space] // no, one or more spaces
			)
			>>
			-omit[CurrencyLong_] // long currency presentation (USD)
			>>
			lit(L")")
		;

		// get a tournament header
		settourneygameheader.name("get tourney game data");
		settourneygameheader =
			(
				no_case[IsTournament_]//[phx::bind(&hhdata::SGameData::istournament, phx::ref(hand)) = _1]
				>>
				*omit[space]
				>>
				lit(L"#")
				>>
				getalphanumstring//[phx::bind(&hhdata::SGameData::tournament_name, phx::ref(hand)) = _1]
				>>
				lit(L",")
				>>
				*omit[space]
			)
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::istournament, phx::ref(hand)) = _1, // set tournament to true, it's a tournament hand
					phx::bind(&hhdata::SGameData::tournament_name, phx::ref(hand)) = _2 // set tournament name
				]
			]
			>>
			*omit[space]
			>>
			(
				(
					omit[no_case[IsFreeRoll_]]
				)
				|
				( // get buyin, bounty and rake if present
					(
						-CurrencyShort_ // the currency
						[
							phx::if_( phx::ref(hand) )
							[
								phx::bind(&hhdata::SGameData::currency, phx::ref(hand)) = _1
							]
						]
						>>
						(
							(
								(
									amount // buyin
									>>
									lit(L"+")
									>>
									-omit[no_case[CurrencyShort_]]
									>>
									amount // get bounty
									>>
									lit(L"+")
									>>
									-omit[no_case[CurrencyShort_]]
									>>
									amount // get rake
								)
								[
									phx::if_( phx::ref(hand) )
									[
										phx::bind(&hhdata::SGameData::tournament_buyin, phx::ref(hand)) = _1,
										phx::bind(&hhdata::SGameData::tournament_bounty, phx::ref(hand)) = _2,
										phx::bind(&hhdata::SGameData::tournament_rake, phx::ref(hand)) = _3
									]
								]
							)
							|
							(
								(
									amount // buyin
									>>
									lit(L"+")
									>>
									-omit[no_case[CurrencyShort_]]
									>>
									amount // get rake
								)
								[
									phx::if_( phx::ref(hand) )
									[
										phx::bind(&hhdata::SGameData::tournament_buyin, phx::ref(hand)) = _1,
										phx::bind(&hhdata::SGameData::tournament_rake, phx::ref(hand)) = _2
									]
								]
							)
						)
						>>
						*omit[space]
						>>
						-omit[CurrencyLong_] // get long term for a currency
					)
				)
				|
				( // tournament is a fpp-tournament, set to $$$$$$$, starts with a simple amount followed by FPP
					omit[amount]
					>>
					no_case[lit(L"fpp")]
					[
						phx::if_( phx::ref(hand) )
						[
							phx::bind(&hhdata::SGameData::currency, phx::ref(hand)) = Definitions::Currency::USD,
							phx::bind(&hhdata::SGameData::tournament_buyin, phx::ref(hand)) = 0 // it's a fpp-tourney, reset buyin to 0
						]
					]
				)
			)
			>>
			*omit[space]
			>>
			(
				settournamentlimittype // set limit type for tournament (mixed hold'em)
				|
				sethandlimittype // get litmit
			)
			>>
			*omit[space]
			>> /*
						lit(L"-") // delimit
						>>
						*omit[space]
						>>
						-omit[getalphanumstring] // MAtch
						>>
						*omit[space]
						>>
						-omit[getalphanumstring] // Round
						>>
						*omit[space]
						>>
						omit[getalphanumstring] // I or II (match round number)
						>>
						omit[getalphanumstring] // LEVEL
						>>
						*omit[space]
						>>
						omit[getalphanumstring] // II or III (Level)
						>>
						*omit[space]*/
			*(
				!lit(L"(")
				>>
				omit[char_]
			)
			>>
			lit(L"(") //
			>>
			-CurrencyShort_ // short term for currency
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::currency, phx::ref(hand)) = _1 // set currency of hand
				]
			]
			>>
			amount
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::limit1, phx::ref(hand)) = _1 // set limit1
				]
			]
			>>
			*omit[space]
			>>
			lit(L"/") // delimit
			>>
			*omit[space]
			>>
			-omit[CurrencyShort_] // omit the next currency
			>>
			amount
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::limit2, phx::ref(hand)) = _1 // set limit2
				]
			]
			>>
			*omit[space]
			>>
			-omit[CurrencyLong_]
			>>
			lit(L")")
		;

		// get the game header for tourney or cashgame
		getgameheader.name("get the game header");
		getgameheader =
			*omit[space]
			>>
			lit(L"#") // start for handname
			>>
			sethandname // get and set the handname #3784237846278364:
			>>
			lit(L":") // handname is over
			>>
			*omit[space]
			>>
			(
				setcashgameheader // get cash game header
				|
				settourneygameheader // try the tournament header
			)
			[
				phx::bind(&hhdata::SGameData::provider, phx::ref(hand)) = Definitions::Providers::POKERSTARS,
				phx::ref(this->GAMESTATE) = Definitions::GameStates::PREFLOP
			]
			>>
			*omit[space]
			>>
			lit(L"-")
			>>
			*omit[space]
			>>
			getdate // get a date
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::datestamp, phx::ref(hand)) = _1 // set the hand date
				]
			]
			>>
			*omit[space]
			>>
			gettime // get a time
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::timestamp, phx::ref(hand)) = _1 // set the hand time
				]
			]
			>>
			*omit[space]
			>>
			omit[getalphanumstring] // CET
			>>
			*omit[space]
			>>
			-(
				lit(L"[") // opener for second date and time
				>>
				getdate // get a date
				[
					phx::if_( phx::ref(hand) )
					[
						phx::bind(&hhdata::SGameData::datestamp, phx::ref(hand)) = _1 // set the hand date
					]
				]
				>>
				*omit[space]
				>>
				gettime // get a time
				[
					phx::if_( phx::ref(hand) )
					[
						phx::bind(&hhdata::SGameData::timestamp, phx::ref(hand)) = _1 // set the hand time
					]
				]
				>>
				*omit[space]
				>>
				omit[getalphanumstring] // ET
				>>
				*omit[space]
				>>
				lit(L"]") // end for second date and time
			)
			>>
			eoi
		;

		// get the limit type
		sethandlimittype.name("get the hand limit");
		sethandlimittype =
			no_case[HandLimitTypes_]
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::limittype, phx::ref(hand)) = _1, //,
					phx::bind(&hhdata::SGameData::tournament_limittype, phx::ref(hand)) = _1 //,
					//phx::if_(phx::ref(hand->tournament_limittype) < 0)
					//[
					//	phx::bind(&hhdata::SGameData::tournament_limittype, phx::ref(hand)) = _1
					//]
				]
			]
		;

		// get the limit type
		settournamentlimittype.name("get the tournament limit");
		settournamentlimittype =
			(
				no_case[TournamentLimitTypes_]
				>>
				*omit[space]
				>>
				lit(L"(")
				>>
				no_case[HandLimitTypes_]
				>>
				*omit[space]
				>>
				lit(L")")
			)
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::tournament_limittype, phx::ref(hand)) = _1,
					phx::bind(&hhdata::SGameData::limittype, phx::ref(hand)) = _2
				]
			]
		;

		// get the hand name
		sethandname.name("get the name of the hand");
		sethandname =
			getalphanumstring
			[
				phx::if_( phx::ref(hand) )
				[
					std::wcout << _1,
					phx::bind(&hhdata::SGameData::gamename, phx::ref(hand)) = _1
				]
			]
		;

		// get playeractions and push them back
		setplayeraction.name("get some player actions");
		setplayeraction =
			(
				playerinitseataction
				[
					phx::if_( phx::ref(hand) )
					[
						//hand->playeractions.push_back(_1);
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
				|
				foldaction // a simple action: fold, muck, check
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
				|
				checkaction // a simple action: fold, muck, check
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
				|
				muckaction // a simple action: fold, muck, check
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
				|
				showaction // a show action with card info
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
				|
				muckwithcardsaction // a muck with card info at summary
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
				|
				raiseaction // raises
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
				|
				collectaction // collecting a pot (collects, wins)
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
				|
				callaction // call a bet
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
				|
				blindaction // posting bigblind/smallblind/dead blind
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
				|
				anteaction // ante
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
				|
				betaction // player bets $1
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
				|
				dealtcardsaction
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
				|
				uncalledbetaction // bet returned to player
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _1)
					]
				]
			)
		;

		// get a game state
		setgamestate.name("set the current gamestate");
		setgamestate =
			getgamestate
			[
				phx::ref(this->GAMESTATE) = _1
			]
		;

		// get turn or river or bla as dealeraction
		setdealeraction.name("gets a game action like dealer cards and so on, a dealeraction");
		setdealeraction = 
			setgamestate
			[
				at_c<0>(_a) = L"DEALER",
				at_c<1>(_a) = Definitions::Actions::DEALER,
				at_c<5>(_a) = ref(this->GAMESTATE),
				phx::if_( phx::ref(hand) )
				[
					phx::push_back(phx::bind(&hhdata::SGameData::playeractions, phx::ref(hand)), _a)
				]
			]
		;

		// get and set board/community card
		setboardcards.name("get and set the community cards");
		setboardcards =
			getcards
			[
				phx::bind(&hhdata::SGameData::CommunityCards, phx::ref(hand)) = _1
			]
		;

		// starting rule for providergrammar
		grammarstart.name("here starts pokerstars");
		grammarstart =
			setplayeraction
			|
			settournamentresult
			|
			(
				setdealeraction
				>>
				*omit[space]
				>>
				-setboardcards
			)
			|
			(
				settablename
				>>
				lit(L"'")
				>>
				*omit[space]
				>>
				setmaxseats
				>>
				*omit[space]
				>>
				setbuttonseat
			)
			|
			setrake
			|
			getgameheader
		;

		// get the name of table
		gettablename.name("get the table-name");
		gettablename =
			omit[no_case[Table_]]
			>>
			*omit[space]
			>>
			lit(L"'")
			>
			*(
				!lit(L"'")
				>
				char_
			)
		;

		// get max number of seats
		getmaxseats.name("maximum seats");
		getmaxseats =
			int_
			>>
			lit(L"-")
			>>
			lit(L"max")
		;

		// get button seat
		getbuttonseat.name("get the dealer seat");
		getbuttonseat =
			-lit(L"(Play Money)")
			>>
			*omit[space]
			>>
			omit[no_case[Seat_]]
			>>
			*omit[space]
			>>
			lit(L"#")
			>>
			int_
			>>
			*omit[space]
			>>
			omit[no_case[ButtonSeat_]]
		;

		// set table name
		settablename.name("set the table name");
		settablename =
			gettablename
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::tablename, phx::ref(hand)) = _1
				]
			]
		;

		// set max seats
		setmaxseats.name("set max seats");
		setmaxseats =
			getmaxseats
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::maxseats, phx::ref(hand)) = _1
				]
			]
		;

		// set button seat
		setbuttonseat.name("get and set buttomn");
		setbuttonseat =
			getbuttonseat
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::buttonseat, phx::ref(hand)) = _1
				]
			]
		;

		// get the rake
		getrake.name("get game rake");
		getrake = 
			lit(L"|")
			>>
			*omit[space]
			>>
			no_case[lit(L"rake")]
			>>
			*omit[space]
			>>
			omit[CurrencyShort_]
			>>
			amount
		;

		// set the rake
		setrake.name("get the game rake");
		setrake = 
			(
				+(
					omit[char_]
					-
						(
							(
								omit[getrake]
								>>
								!omit[getrake]
							)
						)
				)
				>>
				(
					(
						getrake
						>>
						!omit[getrake]
					)
				)
			)
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::gamerake, phx::ref(hand)) = _1
				]
			]
		;

		// tournament winning amount
		tournamentwinamount.name("get winning amount in tournament");
		tournamentwinamount =
			omit[no_case[ReceiveAction_]]
			>>
			*omit[space]
			>>
			omit[no_case[CurrencyShort_]]
			>>
			amount
			[
				_val = _1
			]
		;

		// tournament result in handhistory
		tournamentresultaction.name("get tournament result");
		tournamentresultaction =
			ParserSymbols::PLAYERNAMES
			[
				//at_c<0>(_val) = this->hand->tournament_name,
				at_c<1>(_val) = _1 //,
				//at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>	
			*omit[space]
			>>
			omit[no_case[ReceiveAction_]]
			>>
			*omit[space]
			>>
			-(
				int_
				[
					at_c<5>(_val) = _1
				]
				>>
				+omit[no_case[char_(L"a-zA-Z")]]
				>>
				*omit[space]
				>>
				lit(L"place")
				>>
				*omit[space]
				>>
				*(
					omit[no_case[lit(L"and received")]]
					>>
					*omit[space]
					>>
					omit[no_case[CurrencyShort_]]
					>>
					amount
					[
						at_c<2>(_val) = _1
					]
				)
				>>
				*lit(L".")
			)
			>>
			-(
				omit[no_case[CurrencyShort_]]
				>>
				amount
				[
					at_c<2>(_val) = _1,
					at_c<5>(_val) = 1
				]
				>>
				*omit[space]
				>>
				lit(L"-")
				>>
				*omit[space]
				>>
				omit[no_case[KeyWords_]]
				>>
				*lit(L"!")
			)
			>>
			*omit[char_]
			>>
			eoi
		;

		// fetch a rebuy action
		rebuyaction.name("get rebuy from player");
		rebuyaction = 
			ParserSymbols::PLAYERNAMES
			[
				//at_c<0>(_val) = this->hand->tournament_name,
				at_c<1>(_val) = _1 //,
				//at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			omit[space]
			>>
			omit[no_case[RebuyAction_]]
			>>
			omit[space]
			>>
			omit[amount]
			>>
			omit[space]
			>>
			no_case[lit(L"chips for")]
			>>
			omit[space]
			>>
			omit[no_case[CurrencyShort_]]
			>>
			amount
			[
				at_c<6>(_val) = _1 //,
			]
			>>
			*omit[char_]
			>>
			eoi
		;


		// fetch a bounty action
		bountyaction.name("get bounty for tournament/player");
		bountyaction = 
			ParserSymbols::PLAYERNAMES
			[
				//at_c<0>(_val) = this->hand->tournament_name,
				at_c<1>(_val) = _1 //,
				//at_c<5>(_val) = ref(this->GAMESTATE)
			] // the playername
			>>
			+omit[space]
			>>
			omit[no_case[BountyAction_]]
			>>
			*omit[space]
			>>
			omit[no_case[CurrencyShort_]]
			>>
			amount
			[
				at_c<4>(_val) = _1 //,
			]
			>>
			*omit[space]
			>>
			no_case[lit(L"bounty")]
			>>
			*omit[char_]
			>>
			eoi
		;

		// push the tourneyresult to vector
		settournamentresult.name("extract bounty or tournament result for player");
		settournamentresult =
			tournamentresultaction // got tourneyresult for player
			[
				phx::if_( phx::ref(hand) )
				[
					phx::push_back(phx::bind(&hhdata::SGameData::tournamentresults, phx::ref(hand)), _1)
				]
			]
			|
			bountyaction // got bounty for player
			[
				phx::if_( phx::ref(hand) )
				[
					phx::push_back(phx::bind(&hhdata::SGameData::tournamentresults, phx::ref(hand)), _1)
				]
			]
			|
			rebuyaction // got a rebuy for a player
			[
				phx::if_( phx::ref(hand) )
				[
					phx::push_back(phx::bind(&hhdata::SGameData::tournamentresults, phx::ref(hand)), _1)
				]
			]
		;
		//;


			//debug(actionnocost);
			//debug(playerinitseataction);
	}
}

#endif // POKERSTARSGRAMMAR_DEF_H
