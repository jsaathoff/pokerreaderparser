#ifndef POKERSTARSTOURNAMENTGRAMMAR_H
#define POKERSTARSTOURNAMENTGRAMMAR_H

#include "../includes.hpp"
#include "../standardtypes.hpp"
#include "../standardgrammar/standardpokergrammar.hpp"

namespace pokergrammar
{
	template <typename Iterator>
	struct pokerstarstournamentgrammar : pokergrammar::standardpokergrammar<Iterator>
	{
		pokerstarstournamentgrammar();

		rule<Iterator, hhdata::SPlayerTournamentValues(), locals<std::wstring>> tournamentresultaction;
		rule<Iterator, double()> resultwinning;
		rule<Iterator> settournamentresult;
		rule<Iterator> setbuyin;
		rule<Iterator> setprizepool;
		rule<Iterator, space_type> settournamentstart;
		rule<Iterator, space_type> settournamentend;
		rule<Iterator> setplayercount;
		rule<Iterator, locals<std::wstring>> getplayername;
		rule<Iterator, hhdata::SPlayerTournamentValues()> tournamentresultaction2;

		symbols<wchar_t, bool> ReceiveAction_;
		symbols<wchar_t, bool> BuyIn_;
		symbols<wchar_t, bool> PrizePool_;
		symbols<wchar_t, bool> TournamentStart_;
		symbols<wchar_t, bool> TournamentEnd_;
		symbols<wchar_t, bool> TournamentFinish_;
		symbols<wchar_t, bool> Salutation_;
		symbols<wchar_t, bool> Players_;

		std::wstring ownplayername;
	};
}


#endif // POKERSTARSGRAMMAR_H
