#if defined(_MSC_VER)
# pragma warning(disable: 4345)
#endif

#include "pokerstarsgrammar_def.hpp"

typedef std::wstring::const_iterator iterator_type;
template struct pokergrammar::pokerstarsgrammar<iterator_type>;
