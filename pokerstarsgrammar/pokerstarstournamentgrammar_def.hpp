#ifndef POKERSTARSTOURNAMENTGRAMMAR_DEF_H
#define POKERSTARSTOURNAMENTGRAMMAR_DEF_H

#include "../includes.hpp"
#include "pokerstarstournamentgrammar.hpp"


namespace pokergrammar
{
	template <typename Iterator>
	pokerstarstournamentgrammar<Iterator>::pokerstarstournamentgrammar() //: pokerstarsgrammar::base_type(start)
	{
		// INIT LIMITTYPES
		TournamentLimitTypes_.add(L"no limit hold'em", Definitions::Limits::HOLDEMNOLIMIT);
		TournamentLimitTypes_.add(L"limit hold'em", Definitions::Limits::HOLDEMLIMIT);
		TournamentLimitTypes_.add(L"pot limit hold'em", Definitions::Limits::HOLDEMPOTLIMIT);
		TournamentLimitTypes_.add(L"mixed hold'em", Definitions::Limits::HOLDEMMIXED);
		// INIT LIMITTYPES

		// INIT BUYIN
		BuyIn_.add(L"buy-in:", true);
		// INIT BUYIN
		
		// INIT PRIZEPOOL
		PrizePool_.add(L"total prize pool:", true);
		PrizePool_.add(L"preispool gesamt:", true);
		// INIT PRIZEPOOL

		// INIT Start and End
		TournamentStart_.add(L"tournament started", true);
		TournamentEnd_.add(L"tournament finished", true);
		
		TournamentStart_.add(L"turnierbeginn", true);
		TournamentEnd_.add(L"turnierende", true);
		// INIT Start and End

		// INIT tournament finish
		TournamentFinish_.add(L"you finished the tournament in", true);
		TournamentFinish_.add(L"sie haben in diesem turnier platz", true);
		// INIT tournament finish

		// INIT PLAYERs
		Players_.add(L"players", true);
		Players_.add(L"spieler", true);
		// INIT PLAYERs

		// INIT Salutation
		Salutation_.add(L"dear", true);
		Salutation_.add(L"hallo", true);
		// INIT Salutation

		// INIT Keywords
		KeyWords_.add(L"place", true);
		KeyWords_.add(L"a", true);
		KeyWords_.add(L"erreicht", true);
		KeyWords_.add(L"das preisgeld in h�he von", true);
		// INIT Keywords
		
		// get a tournament header
		settourneygameheader.name("get tourney game data");
		settourneygameheader = 
			(
				*omit[space]
				>>
				lit(L"#")
				>>
				getalphanumstring
				>>
				lit(L",")
				>>
				*omit[space]
				>>
				no_case[TournamentLimitTypes_]
				>>
				*omit[space]
				>>
				eoi
			)
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::istournament, phx::ref(hand)) = true, // set tournament to true, it's a tournament hand
					phx::bind(&hhdata::SGameData::tournament_name, phx::ref(hand)) = _1, // set tournament name
					phx::bind(&hhdata::SGameData::tournament_limittype, phx::ref(hand)) = _2 // set tournament limit type
				]
			]
		;

		// get buy-in line, cock ball torture
		setbuyin.name("get buy-in");
		setbuyin =
			omit[no_case[BuyIn_]]
			>>
			*omit[space]
			>>
			-CurrencyShort_ // the currency
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::currency, phx::ref(hand)) = _1
				]
			]
			>>
			amount // get buyin
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::tournament_buyin, phx::ref(hand)) = _1
				]
			]
			>>
			lit(L"/")
			>>
			-omit[no_case[CurrencyShort_]]
			>>
			amount // get rake or bounty
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::tournament_rake, phx::ref(hand)) = _1
				]
			]
			>>
			-(
				lit(L"/")
				>>
				-omit[no_case[CurrencyShort_]]
				>>
				amount // get bounty
				[
					phx::if_( phx::ref(hand) )
					[
						phx::bind(&hhdata::SGameData::tournament_bounty, phx::ref(hand)) = phx::ref(hand->tournament_rake),
						phx::bind(&hhdata::SGameData::tournament_rake, phx::ref(hand)) = _1,
						phx::bind(&hhdata::SGameData::tournament_buyin, phx::ref(hand)) += phx::ref(hand->tournament_bounty)
					]
				]
			)
			>>
			*omit[space]
			>>
			omit[no_case[CurrencyLong_]]
			>>
			*omit[char_]
			>>
			eoi
		;

		// get and set total playercount
		setplayercount.name("get total playercount");
		setplayercount =
			(
				int_
				>>
				*omit[space]
				>>
				omit[no_case[Players_]]
				>>
				*omit[space]
				>>
				*omit[char_]
				>>
				eoi
			)
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::tournament_playercount, phx::ref(hand)) = _1
				]
			]
		;

		// get total prizepool
		setprizepool.name("get and set prizepool");
		setprizepool =
			omit[no_case[PrizePool_]]
			>>
			*omit[space]
			>>
			-CurrencyShort_ // the currency
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::currency, phx::ref(hand)) = _1
				]
			]
			>>
			amount // get buyin
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::tournament_prizepool, phx::ref(hand)) = _1
				]
			]
			>>
			*omit[space]
			>>
			omit[no_case[CurrencyLong_]]
			>>
			*omit[char_]
			>>
			eoi
		;

		// start date/time of tournament
		settournamentstart.name("set start date/time for tournament");
		settournamentstart =
			(
				omit[no_case[TournamentStart_]]
				>>
				*omit[space]
				>>
				getdate
				>>
				*omit[space]
				>>
				gettime
				>>
				*omit[char_]
				>>
				eoi
			)
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::tournament_startdate, phx::ref(hand)) = _1,
					phx::bind(&hhdata::SGameData::tournament_starttime, phx::ref(hand)) = _2
				]
			]
		;

		// end date/time of tournament
		settournamentend.name("set end date/time for tournament");
		settournamentend =
			(
				omit[no_case[TournamentEnd_]]
				>>
				*omit[space]
				>>
				getdate
				>>
				*omit[space]
				>>
				gettime
				>>
				*omit[char_]
				>>
				eoi
			)
			[
				phx::if_( phx::ref(hand) )
				[
					phx::bind(&hhdata::SGameData::tournament_enddate, phx::ref(hand)) = _1,
					phx::bind(&hhdata::SGameData::tournament_endtime, phx::ref(hand)) = _2
				]
			]
		;

		// fetch a tournament-result
		tournamentresultaction.name("get tournament result for player");
		tournamentresultaction = 
			*omit[space]
			>>
			int_[at_c<5>(_val) = _1]
			>>
			lit(L":")
			>>
			omit[space]
			>>
			+(
				lexeme[char_[_a += _1]] -  // playername
				(
					(
						omit[resultwinning]
						>>
						!omit[resultwinning]
					)
				)
			)
			>>
			(
				(
					resultwinning[at_c<2>(_val) = _1]
					>>
					!omit[resultwinning]
				)
			)
			[at_c<1>(_val) = _a]
		;

		// fetch the tournament winning if possible
		resultwinning.name("get winning for player");
		resultwinning = 
			space
			>>
			lit(L"(")
			>>
			+omit[~char_(L")")]
			>>
			lit(L")")
			>>
			lit(L",")
			>>
			*(
				*omit[space]
				>>
				omit[CurrencyShort_]
				>>
				amount[_val = _1] // the amount/winning
			)
			>>
			*omit[char_]
		;


		// get a result for player in tournament
		// push the tourneyresult to vector
		settournamentresult.name("get result and push it");
		settournamentresult =
			(
				tournamentresultaction // got tourneyresult for player
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::tournamentresults, phx::ref(hand)), _1)
					]
				]
			)
			|
			(
				tournamentresultaction2
				[
					phx::if_( phx::ref(hand) )
					[
						phx::push_back(phx::bind(&hhdata::SGameData::tournamentresults, phx::ref(hand)), _1)
					]
				]
			)
			
		;

		// get result for tourney for player
		tournamentresultaction2.name("get result");
		tournamentresultaction2 = 
			omit[no_case[Salutation_]]
			>>
			*omit[space]
			>>
			int_
			[
				at_c<5>(_val) = _1, // place in tournament
				at_c<1>(_val) = phx::ref(ownplayername)
			]
			>>
			*omit[char_(L"a-zA-Z0-9.")]
			>>
			*omit[space]
			>>
			omit[no_case[KeyWords_]]
			>>
			*lit(".")
			>>
			-(
				*omit[space]
				>>
				omit[KeyWords_]
				>>
				*omit[space]
				>>
				omit[CurrencyLong_]
				>>
				*omit[space]
				>>
				amount[at_c<2>(_val) = _1]
			)
		;

		// get playername in dear blablal, 
		getplayername.name("get playername");
		getplayername = 
			omit[no_case[Salutation_]]
			>>
			omit[space]
			>>
			+(
				lexeme[char_[_a += _1]] -  // playername
				(
					(
						lit(L",")
						>>
						!lit(L",")
					)
				)
			)
			>>
			(
				lit(L",")
				>>
				!lit(L",")
			)
			[
				phx::bind(&ParserSymbols::addownplayername_asstring, _a),
				phx::ref(ownplayername) = _a
			]
		;

		grammarstart.name("grammar main entry");
		grammarstart = 
			settourneygameheader
			|
			setprizepool
			|
			setbuyin
			|
			settournamentstart
			|
			settournamentend
			|
			setplayercount
			|
			getplayername
			|
			settournamentresult
		;
	}
}

#endif // POKERSTARSTOURNAMENTGRAMMAR_DEF_H
