#include "includes.hpp"
#include "grammarcontainer.h"
#include "standardtypes.hpp"
#include "hhdata.h"


int main(int argc, char *argv[])
{
	std::wstring::const_iterator iterbegin;
	std::wstring::const_iterator iterend;
	std::wstring subject1, subject2, subject3;
	typedef std::wstring::const_iterator iterator_type;
	//template struct grammar::standardgrammar::standardgrammar<iterator_type>;
	//pokergrammar::textpokergrammar<iterator_type> gramm;
	GrammarContainer grammcont;
	hhdata::HandContainer cont;// = new hhdata::HandContainer();
	grammcont.gramm.setContainer(&cont);

	//hhdata::HandContainer * hcon = new hhdata::HandContainer();
	//gramm.SetHandContainer(hcon);

	//hhdata::SPlayerAction playeraction1;
	//hhdata::SPlayerAction playeraction2;
	datatypes::Cards board;

	std::wstring name;


	std::vector<std::wstring> lines;
	lines.push_back(L"PokerStars Hand #81861768752: Tournament #573691557, $6.68+$0.45 USD Hold'em No Limit - Level I (15/30) - 2012/06/12 20:11:31 CET [2012/06/12 14:11:31 ET]");
	lines.push_back(L"Seat 2: testname (1200 in chips)");
	lines.push_back(L"testname: checks");
	lines.push_back(L"Seat 2: testname (button) mucked [Kh 6d]");
	lines.push_back(L"*** FLOP *** [2d Kh 6d]");
	lines.push_back(L"Uncalled bet ($0.04) returned to testname");
	lines.push_back(L"testname: raises $0.02 to $0.06");
	lines.push_back(L"testname: calls $0.02");
	lines.push_back(L"testname: bets $0.04");
	lines.push_back(L"testname: posts big blind $0.02");
	lines.push_back(L"testname: shows [Ac Jh]");
	lines.push_back(L"testname: shows [Ac Jh] (two pair)");
	lines.push_back(L"testname collected $0.26 from pot");
	lines.push_back(L"*** RIVER *** [2d Kh 6d Tc] [6s]");
	lines.push_back(L"testname: calls $0.04 and is all-in");
	lines.push_back(L"testname: bets $0.04 and is all-in");
	lines.push_back(L"testname: raises 0.02 to $0.04 and is all-in");
	lines.push_back(L"testname finished the tournament in 90th place and received $101.20.");
	lines.push_back(L"Dealt to testname [Jc 4h]");
	lines.push_back(L"Table 'Urhixidur III' 6-max Seat #4 is the button");
	lines.push_back(L"Total pot $2.55 | Rake $0.10");
	lines.push_back(L"PokerStars Game #26561377266:  Hold'em No Limit ($2/$4) - 2009/03/31 8:44:44 CET [2009/03/31 2:44:44 ET]");
	lines.push_back(L"Seat 2: testname (1200 in chips)");
	lines.push_back(L"testname: checks");
	lines.push_back(L"Seat 2: testname (button) mucked [Kh 6d]");
	lines.push_back(L"*** FLOP *** [2d Kh 6d]");
	lines.push_back(L"Uncalled bet ($0.04) returned to testname");
	lines.push_back(L"testname: raises $0.02 to $0.06");
	lines.push_back(L"testname: calls $0.02");
	lines.push_back(L"testname: bets $0.04");
	lines.push_back(L"testname: posts big blind $0.02");
	lines.push_back(L"testname collected $0.26 from pot");
	lines.push_back(L"*** RIVER *** [2d Kh 6d Tc] [6s]");
	lines.push_back(L"testname: calls $0.04 and is all-in");
	lines.push_back(L"testname: bets $0.04 and is all-in");
	lines.push_back(L"testname: raises 0.02 to $0.04 and is all-in");
	lines.push_back(L"Dealt to testname [Jc 4h]");
	lines.push_back(L"Table 'Urhixidur III' 6-max Seat #4 is the button");
	lines.push_back(L"Total pot $2.55 | Rake $0.10");
	lines.push_back(L"PokerStars Game #26561377266:  Hold'em No Limit ($2/$4) - 2009/03/31 8:44:44 CET [2009/03/31 2:44:44 ET]");
	lines.push_back(L"Dear sevenwiser,");
	lines.push_back(L"You finished the tournament in 1st place. A USD 20.00 award has been credited to your Real Money account.");
	lines.push_back(L"testname wins the $4 bounty for eliminating heinz");
	lines.push_back(L"testname wins the tournament and receives $21.34 - congratulations!");
	lines.push_back(L"testname finished the tournament in 2nd place and received $13.34.");


	for(int i = 0; i<lines.size(); i++)
	{
		iterend = lines[i].end();
		iterbegin = lines[i].begin();

		boost::spirit::qi::phrase_parse(iterbegin, iterend, grammcont.gramm, boost::spirit::standard_wide::space);
	}

	cont.CalcGames();

	/*
	subject = L"nkgeorge62 n: folds";

		iterend = subject.end();
		iterbegin = subject.begin();

		boost::spirit::qi::phrase_parse(iterbegin, iterend, gramm., boost::spirit::standard_wide::space, playeraction2);
		*/

	/*if(ParserSymbols::PLAYERNAMES.find(L"nkgeorge62 n") == NULL)
	{
		int i = 0;
		i++;
	}*/

	//std::wcout << L"\n\n" << name << L"\n";
	getchar();
	return 0;
}


/*
#include<iostream>
#include<string>
#include<boost/spirit/include/qi.hpp>
#include<boost/spirit/include/phoenix.hpp>

namespace phx = boost::phoenix;
namespace qi  = boost::spirit::qi;

using qi::char_;
using qi::_1;
using qi::_r1;
using std::cout;
using std::endl;

void testing(char a){ cout << a << endl; }

struct test_grammar : qi::grammar<std::string::iterator>
{
	test_grammar() : test_grammar::base_type(start)
	{
		start = expression(true);
		expression = char_('a')[
			phx::if_(_r1)
				[
					phx::bind(&testing, qi::_1 )
				]
		];

	}

	qi::rule<std::string::iterator> start;
	qi::rule<std::string::iterator, void(bool)> expression;

};

int main(void)
{
	std::string str("a");

	test_grammar tg;

	std::string::iterator start_ = str.begin();
	parse(start_, str.end(), tg);

	return 1;
}*/
