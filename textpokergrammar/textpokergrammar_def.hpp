#ifndef TEXTPOKERGRAMMAR_DEF_H
#define TEXTPOKERGRAMMAR_DEF_H

#include "../includes.hpp"
#include "textpokergrammar.hpp"


namespace pokergrammar
{
	template <typename Iterator>
	textpokergrammar<Iterator>::textpokergrammar() : textpokergrammar::base_type(start)
	{
		hand = NULL;

		// INIT BOM_
		wchar_t const* w1 = L"\ufeff";
		wchar_t const* w2 = L"\ufffe";
		Bom_.add(w1);
		Bom_.add(w2);
		// INIT BOM_

		// INIT PROVIDERS
		//Provider_.add(L"full tilt poker game", Definitions::Providers::FULLTILT);
		Provider_.add(L"pokerstars game", Definitions::Providers::POKERSTARS);
		Provider_.add(L"pokerstars hand", Definitions::Providers::POKERSTARS);
		Provider_.add(L"pokerstars zoom hand", Definitions::Providers::POKERSTARS);
		Provider_.add(L"pokerstars tournament", Definitions::Providers::TOURNAMENTSUMMARYPS);
		Provider_.add(L"pokerstars turnier", Definitions::Providers::TOURNAMENTSUMMARYPS);
		// INIT PROVIDERS

		getinput =
			(
				*char_[_a += _1]
			)
			[
				phx::ref(inputtext) += _a,
				phx::ref(inputtext) += L"\n",
				phx::ref(inputtmptext) = _a,
				phx::ref(inputtmptext) += L"\n"
			]
		;

		start = 
			&getinput
			>>
			(
				-Bom_
			)
			[
				phx::if_(phx::ref(PROVIDER) == Definitions::Providers::POKERSTARS)
				[
					_a = PokerStarsGramm.grammarstart
				]
				.else_
				[
					_a = PokerStarsTournamentGrammar.grammarstart
				]
			]
			>>
			(
				setprovider
				[
					phx::if_(phx::ref(PROVIDER) == Definitions::Providers::POKERSTARS)
					[
						phx::if_(phx::ref(hand))
						[
							phx::bind(&pokergrammar::textpokergrammar<Iterator>::replaceString, phx::ref(this), &inputtext, &inputtmptext),
							phx::bind(&hhdata::SGameData::HHTEXTORG, phx::ref(hand)) = phx::ref(inputtext),
							phx::bind(&hhdata::SGameData::m_sFilename, phx::ref(hand)) = phx::ref(m_fileName),
							phx::ref(inputtext) = phx::ref(inputtmptext),
							phx::push_back(phx::bind(&hhdata::HandContainer::hands, phx::ref(handcontainer)), phx::ref(hand))
						],
						phx::ref(hand) = phx::new_<hhdata::SGameData>(), //,
						phx::ref(PokerStarsGramm.hand) = phx::ref(hand), //, //, //, //, //,
						phx::bind(&hhdata::SGameData::provider, phx::ref(hand)) = Definitions::Providers::POKERSTARS,
						_a = PokerStarsGramm.grammarstart
					],
					phx::if_(phx::ref(PROVIDER) == Definitions::Providers::TOURNAMENTSUMMARYPS)
					[
						phx::if_(phx::ref(hand))
						[
							phx::bind(&pokergrammar::textpokergrammar<Iterator>::replaceString, phx::ref(this), &inputtext, &inputtmptext),
							phx::bind(&hhdata::SGameData::HHTEXTORG, phx::ref(hand)) = phx::ref(inputtext),
							phx::bind(&hhdata::SGameData::m_sFilename, phx::ref(hand)) = phx::ref(m_fileName),
							phx::ref(inputtext) = phx::ref(inputtmptext),
							phx::push_back(phx::bind(&hhdata::HandContainer::hands, phx::ref(handcontainer)), phx::ref(hand))
						],
						phx::ref(hand) = phx::new_<hhdata::SGameData>(), //,
						phx::ref(PokerStarsTournamentGrammar.hand) = phx::ref(hand), //, //, //, //, //,
						phx::bind(&hhdata::SGameData::provider, phx::ref(hand)) = Definitions::Providers::TOURNAMENTSUMMARYPS,
						_a = PokerStarsTournamentGrammar.grammarstart
					]
				]
				>>
				lazy(_a)
			)
			|
			(
				lazy(_a) // _a contains provider rule
			)
		;

		setprovider = 
			no_case[Provider_]
			[
				phx::ref(PROVIDER) = _1
			]
		;

		debug(start);
		
	}
}

#endif // TEXTPOKERGRAMMAR_DEF_H
