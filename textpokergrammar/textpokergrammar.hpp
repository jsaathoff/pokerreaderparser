#ifndef TEXTPOKERGRAMMAR_H
#define TEXTPOKERGRAMMAR_H

#include "../includes.hpp"
#include "../standardtypes.hpp"
#include "../pokerstarsgrammar/pokerstarsgrammar.hpp"
#include "../pokerstarsgrammar/pokerstarstournamentgrammar.hpp"
#include "../hhdata.h"


namespace pokergrammar
{
	/*
	using namespace boost::spirit::qi::standard_wide;
		namespace qi = boost::spirit::qi;
		using qi::symbols;
		using qi::omit;
		using qi::lit;
		using qi::int_;
		using qi::double_;
		using qi::as_wstring;
		using qi::locals;
		using qi::rule;
		using qi::_pass;
		using qi::eoi;
		using qi::lexeme;
	
		using qi::_1;
		using qi::_2;
		using qi::_3;
		using qi::_4;
		using qi::_5;
		using qi::_6;
		using qi::_7;
	
		using qi::_a;
		using qi::_b;
		using qi::_c;
		using qi::_d;
		using qi::_e;
		using qi::_f;
		using qi::_g;
	
		using qi::repeat;
	
		using qi::_val;
	
		//using qi::standard_wide::char_;
		using qi::standard_wide::no_case;
		using qi::standard_wide::alpha;
		using qi::standard_wide::space;
		using qi::standard_wide::space_type;
	
		using namespace boost::phoenix;
		namespace phx = boost::phoenix;
		using boost::phoenix::at_c;
		using boost::phoenix::if_;
		using boost::phoenix::else_gen;
	
		using boost::fusion::push_back;
	
		//typedef std::wstring::const_iterator iterator_type;
		//template struct pokergrammar::pokerstarsgrammar<iterator_type>;
	
		
	
		//PokerStarsGramm psgrammar;
	
		using namespace boost::spirit::qi::standard_wide;
		namespace qi = boost::spirit::qi;*/
	

	template <typename Iterator>
	struct textpokergrammar : qi::grammar<Iterator, space_type, qi::locals<qi::rule<Iterator, space_type>> >
	{
		textpokergrammar();
		pokergrammar::pokerstarsgrammar<Iterator> PokerStarsGramm;
		pokergrammar::pokerstarstournamentgrammar<Iterator> PokerStarsTournamentGrammar;
		
		void setContainer(hhdata::HandContainer * cont)
		{
			handcontainer = cont;
			PokerStarsGramm.setContainer(cont);
		}

		void SetFileName(std::wstring parsefilename)
		{
			m_fileName = parsefilename;
		}

		std::wstring GetFileName()
		{
			return m_fileName;
		}

		void replaceString(std::wstring * org, std::wstring * replacer)
		{
			size_t nPos;
			
			nPos = org->find(replacer->c_str());
			org->replace(nPos, replacer->length(), L"");
		}

		hhdata::HandContainer * handcontainer;
		hhdata::SGameData * hand;

		rule<Iterator, space_type, qi::locals<qi::rule<Iterator, space_type>> > start;
		rule<Iterator, space_type> providerrule;
		rule<Iterator, boost::spirit::unused_type, space_type> getproviderheader;

		// set the provider
		rule<Iterator, boost::spirit::unused_type, space_type> setprovider;
		// set the provider
		rule<Iterator, hhdata::SPlayerAction(), space_type> seataction;

		rule<Iterator, locals<std::wstring> > getinput;

		symbols<wchar_t, unsigned> Provider_;
		symbols<wchar_t, unsigned> Bom_;

		std::wstring inputtext;
		std::wstring inputtmptext;
		std::wstring m_fileName;

		int PROVIDER;
	};
}


#endif // TEXTPOKERGRAMMAR_H
