#ifndef STANDARDTYPES_H 
#define STANDARDTYPES_H 

#include <string>
#include <iostream>
#include <stdlib.h>
#include <boost/fusion/sequence.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/cstdint.hpp>
#include <ibpp/src/ibpp/ibpp.h>


namespace datatypes 
{
	struct DateStruct
	{
		DateStruct()
		{
			day = 0;
			month = 0;
			year = 0;
		};

		~DateStruct()
		{
		};

		int day;
		int month;
		int year;
	};

	struct TimeStruct
	{
		~TimeStruct()
		{
		};
		
		TimeStruct()
		{
			hour = 0;
			minute = 0;
			second = 0;
		};

		int minute;
		int hour;
		int second;
	};

	struct Cards
	{
		Cards()
		{
			card1 = L"";
			card2 = L"";
			card3 = L"";
			card4 = L"";
			card5 = L"";
		};

		std::wstring card1;
		std::wstring card2;
		std::wstring card3;
		std::wstring card4;
		std::wstring card5;
	};

	typedef TimeStruct TimeInfo;
	typedef DateStruct DateInfo;
	typedef Cards CardInfo;
	//typedef boost::fusion::vector<std::wstring, std::wstring, std::wstring, std::wstring, std::wstring> PlayerCards;
	typedef int64_t int64;

	namespace qi = boost::spirit::qi;
	namespace standard_wide = boost::spirit::standard_wide;
	namespace phoenix = boost::phoenix;



	///////////////////////////////////////////////////////////////////////////////
	//  These policies can be used to parse thousand separated
	//  numbers with at most 2 decimal digits after the decimal
	//  point. e.g. 123,456,789.01
	///////////////////////////////////////////////////////////////////////////////
	template <typename T>
	struct ts_real_policies : boost::spirit::qi::ureal_policies<T>
	{
		//  2 decimal places Max
		template <typename Iterator, typename Attribute>
		static bool parse_frac_n(Iterator& first, Iterator const& last, Attribute& attr)
		{
			return boost::spirit::qi::
				extract_uint<T, 10, 1, 2, true>::call(first, last, attr);
		}

		//  No exponent
		template <typename Iterator>
		static bool parse_exp(Iterator&, Iterator const&)
		{
			return false;
		}

		//  No exponent
		template <typename Iterator, typename Attribute>
		static bool parse_exp_n(Iterator&, Iterator const&, Attribute&)
		{
			return false;
		}

		//  Thousands separated numbers
		template <typename Iterator, typename Attribute>
		static bool parse_n(Iterator& first, Iterator const& last, Attribute& attr)
		{
			using boost::spirit::qi::uint_parser;
			namespace qi = boost::spirit::qi;

			uint_parser<unsigned, 10, 1, 3> uint3;
			uint_parser<unsigned, 10, 3, 3> uint3_3;
			uint_parser<unsigned, 10, 2, 2> uint2_2;

			T result = 0;
			if (parse(first, last, uint3, result))
			{
				bool hit = false;
				T n;
				Iterator save = first;

				while (qi::parse(first, last, ',') && qi::parse(first, last, uint3_3, n))
				{
					result = result * 1000 + n;
					save = first;
					hit = true;
				}

				if( (first != last) && (qi::parse(first, last, &qi::matches['0' >> qi::int_], n)))
				{
					if( (first != last) && (qi::parse(first, last, uint2_2, n)))
					{
						result = result + (n * 0.01);
					}

					save = first;
					hit = true;
				}
				else if( (first != last) && (qi::parse(first, last, uint2_2, n)))
				{
					if( n > 9)
						result = result + (n * 0.01);
					else
						result = result + (n * 0.1);

					save = first;
					hit = true;
				}

				first = save;
				if (hit)
					attr = result;
				return hit;
			}
			return false;
		}
	};

	/*
	struct SPlayerAction
	{
		SPlayerAction()
		{
			playername = L"";
			whataction = -1;
			amount1 = 0;
			amount2 = 0;
			allin = false;
			gamestate = Definitions::GameStates::PREFLOP;
			//cards = datatypes::Cards;
			finalhand = L"";
			pottype = -1;
			seat = 0;
		};

		std::wstring playername;
		int whataction;
		double amount1;
		double amount2;
		bool allin;
		int gamestate;
		datatypes::PlayerCards cards;
		std::wstring finalhand;
		int pottype;
		int seat;
	};

	struct SPlayerValues
	{
		SPlayerValues()
		{
			dbid = -1;
			currentbetpot = 0;
			ante = 0;
			seat = 0;
			startamount = 0;
			name = L"";
			position = 0;
			ats = -1;
			vpip = 0;
			pfr = 0;
			r2steal = -1;
			pf_aggressor = false;
			coldcall = -1;
			pf_3bet = -1;
			pf_4bet = -1;
			r2rsteal = -1;
			actioncounter = 0;
			flop_cbet = -1;
			turn_cbet = -1;
			river_cbet = -1;
			flop_donkbet = -1;
			turn_donkbet = -1;
			river_donkbet = -1;
			river_float = -1;
			turn_float = -1;
			flop_r2cbet = -1;
			flop_r2donkbet = -1;
			flop_3bet = -1;
			flop_4bet = -1;
			river_r2donkbet = -1;
			turn_r2donkbet = -1;
			turn_r2cbet = -1;
			turn_3bet = -1;
			turn_4bet = -1;
			river_r2cbet = -1;
			river_r2float = -1;
			river_3bet = -1;
			river_4bet = -1;
			turn_r2float = -1;
			pf_bets = 0;
			pf_calls = 0;
			pf_raises = 0;
			pf_checks = 0;
			pf_folds = 0;
			flop_bets = 0;
			flop_calls = 0;
			flop_raises = 0;
			flop_checks = 0;
			flop_folds = 0;
			turn_bets = 0;
			turn_calls = 0;
			turn_raises = 0;
			turn_checks = 0;
			turn_folds = 0;
			river_bets = 0;
			river_calls = 0;
			river_raises = 0;
			river_checks = 0;
			river_folds = 0;
			cost = 0;
			winning = 0;
			lastaction = -1;
			sawflop = 0;
			sawshowdown = 0;
			rake = 0;
			pf_r23bet = -1;
			pf_r24bet = -1;
			smallblind = 0;
			bigblind = 0;
			ownplayer = 0;
			
			finalhand = L"";
			mucked = 0;
			initiate = false;

			pf_allin = 0;
			flop_allin = 0;
			turn_allin = 0;
			river_allin = 0;

			// dead small blind
			deadsmallblind = 0;

			// dead big blind
			deadbigblind = 0;

			showdowns = 0;
		};

		// database id of player
		int64 dbid;

		// was initiated
		bool initiate;

		// player's seat
		int seat;

		// the players own betpot
		double currentbetpot;

		// did the player mucked his cards ?
		int mucked;

		// dead small blind
		double deadsmallblind;

		// dead big blind
		double deadbigblind;

		// start amount in money
		double startamount;

		// player name
		std::wstring name;

		// is it an own player ?
		int ownplayer;

		// the position
		int position;

		// facing
		int facing;

		// actioncounter...
		int actioncounter;

		// attempt to steal
		int ats;

		// volountarily put oney in pot
		int vpip;

		// raised preflop
		int pfr;

		// reaction 2 steal
		int r2steal;

		// is preflop agressor
		int pf_aggressor;

		// cold call value
		int coldcall;

		// ante posted
		double ante;

		// preflop 3bet
		int pf_3bet;

		// preflop 4bet
		int pf_4bet;

		// react to 3 bet
		int pf_r23bet;

		// react to 4bet
		int pf_r24bet;

		// preflop all-in?
		int pf_allin;

		// flop all-in?
		int flop_allin;

		// turn all-in?
		int turn_allin;

		// river all-in?
		int river_allin;

		// reaction 2 re-steal
		int r2rsteal;

		// cbet flop
		int flop_cbet;

		// cbet turn
		int turn_cbet;

		// cbet flop
		int river_cbet;

		// donkbet flop
		int flop_donkbet;

		// donkbet turn
		int turn_donkbet;

		// donkbet river
		int river_donkbet;

		// float turn
		int turn_float;

		// float river
		int river_float;

		// r2cbet on flop
		int flop_r2cbet;

		// reaction to donkbet on flop
		int flop_r2donkbet;

		// flop 4bet
		int flop_4bet;

		// flop 3bet
		int flop_3bet;

		// reaction 2 donkbet on turn
		int turn_r2donkbet;

		// reaction 2 donkbet on river
		int river_r2donkbet;

		// reaction 2 cbet on turn
		int turn_r2cbet;

		// turn 3bet
		int turn_3bet;

		// turn 4bet
		int turn_4bet;

		// river 3bet
		int river_3bet;

		// river 4bet
		int river_4bet;

		// river r2cbet
		int river_r2cbet;

		// river r2float
		int river_r2float;

		// turn r2float
		int turn_r2float;

		// preflop bets
		int pf_bets;

		// preflop calls
		int pf_calls;

		// preflop raises
		int pf_raises;

		// preflop checks
		int pf_checks;

		// preflop folds
		int pf_folds;

		// flop bets
		int flop_bets;

		// flop calls
		int flop_calls;

		// flop raises
		int flop_raises;

		// flop checks
		int flop_checks;

		// flop folds
		int flop_folds;

		// turn bets
		int turn_bets;

		// turn calls
		int turn_calls;

		// trun raises
		int turn_raises;

		// turn checks
		int turn_checks;

		// turn folds
		int turn_folds;

		// river bets
		int river_bets;

		// river calls
		int river_calls;

		// river raises
		int river_raises;

		// river checks
		int river_checks;

		// river folds
		int river_folds;

		// total cost 
		double cost;

		// total winning
		double winning;

		// last action of player
		int lastaction;

		// saw flop ?
		int sawflop;

		// saw showdown
		int sawshowdown;

		// rake paid 
		double rake;

		// small blind
		double smallblind;

		// big blind
		double bigblind;

		// player pocket cards
		Cards holecards;

		// finalhand for player
		std::wstring finalhand;

		int showdowns;
	};

	struct SPlayerTournamentValues
	{
		// destructor
		~SPlayerTournamentValues()
		{

		};

		// constructor
		SPlayerTournamentValues()
		{
			winning = 0;
			bountyamount = 0;
			bounty = 0;
			place = 0;
			rebuy = 0;
			addon = 0;
			rebuyamount = 0;
			addonamount = 0;
		};

		double winning;
		int bountyamount;
		double bounty;

		int place;

		double rebuy;
		int rebuyamount;

		double addon;
		int addonamount;
	};

	
	struct SGameData
	{

		// constructor
		SGameData()
		{
			aggressor = -1;
			atspos = -1;
			flop_cbet_opp = -1;
			turn_cbet_opp = -1;
			donkpos = -1;
			float_pos = -1;
			flop_donk_opp = -1;
			turn_donk_opp = -1;
			river_cbet_opp = -1;
			river_donk_opp = -1;
			turn_float_opp = -1;
			river_float_opp = -1;
			bet3opp = -1;
			bet3pos = -1;
			bet4pos = -1;
			bet4opp = -1;
			// facings
			pf_facing = 0;
			flop_facing = 0;
			turn_facing = 0;
			river_facing = 0;
			pos = 0;
			x = 0;
			i = 0;

			// temp ats opp
			atsopp = -1;

			// was imported
			imported = false;

			// temp position
			tmppos = -1;

			// preflop raised ? 
			pf_raise = false;

			aggressorwasset = false;

			// the smallblind
			smallblind = 0;

			// the bigblind
			bigblind = 0;

			// the player count, facing, position and what was done by player
			playercount = 0;

			// the game duration
			duration = 60;

			// the game identifier
			gamename =L"";

			// game limit type
			limittype = 0;

			// gamepot total
			totalpot = 0;

			// sidepot 
			sidepot = 0;

			//mainpot 
			mainpot = 0;

			// game limit1
			limit1 = 0;

			// game limit2
			limit2 = 0;

			// game rake
			gamerake = 0;

			// tourney rake
			tournament_rake = 0;

			// original hand history text
			HHTEXTORG = L"";

			// database id
			gamedbid = 0;

			// timestamp
			//ibppdatetime = NULL;

			// time and date def
			//hour, minute, second, day, month, year;

			// the poker provider
			provider = 0;

			// max. number of seats
			maxseats = 0;

			// name of table
			tablename = L"";

			// the dealer sits at seat ...
			buttonseat = 0;

			// the current gamestate : PREFLOP,FLOP,TURN,RIVER,SHOWDOWN,SUMMARY
			//gamestate = 0;

			// the currency
			currency = 0;

			tournament_prizepool = 0;

			tournament_rake = 0;

			tournament_rebuy = 0;

			tournament_addon = 0;

			tournament_bounty = 0;

			tournament_limittype = -1;

			// the tourney identifier
			tournament_name = L"";

			// the tourney identifier
			tournament_dbid = 0;

			// the tourney buyin amount
			tournament_buyin = 0;

			// rebuy addon
			tournament_rebuy = 0;

			tournament_startdate = DateInfo();

			tournament_starttime = TimeInfo();

			tournament_endtime = TimeInfo();

			tournament_enddate = DateInfo();

			showdowncounter = 0;

			issummary = false;

			isappended = false;
		};

		// destructor
		~SGameData();

		// timestamp
		TimeInfo timestamp;

		// date
		DateInfo datestamp;

		std::map<std::wstring, SPlayerValues> PlayerValues;
		std::map<std::wstring, SPlayerTournamentValues> PlayerTournamentValues;

		// aggressor position
		int aggressor, atspos, flop_cbet_opp, turn_cbet_opp, donkpos, float_pos, flop_donk_opp, turn_donk_opp, river_cbet_opp, river_donk_opp, turn_float_opp, river_float_opp;
		int bet3opp, bet3pos, bet4pos, bet4opp;
		// facings
		int pf_facing, flop_facing, turn_facing, river_facing, pos;
		size_t x, i;

		// all player actions
		std::vector<SPlayerAction> playeractions;

		// community cards
		Cards CommunityCards;

		// temp ats opp
		int atsopp;

		// was imported
		bool imported;

		// temp position
		int tmppos;

		// preflop raised ? 
		bool pf_raise;

		bool aggressorwasset;

		// the smallblind
		double smallblind;

		// the bigblind
		double bigblind;

		// the player count, facing, position and what was done by player
		int playercount;

		// the game duration
		double duration;

		// the game identifier
		std::wstring gamename;

		// game limit type
		int limittype;

		// gamepot total
		double totalpot;

		// sidepot 
		double sidepot;

		//mainpot 
		double mainpot;

		// game limit1
		double limit1;

		// game limit2
		double limit2;

		// game rake
		double gamerake;

		// gametype (real- playmoney)
		int moneytype;

		// original hand history text
		std::wstring HHTEXTORG;

		// database id
		int64 gamedbid;

		// time and date def
		//int hour, minute, second, day, month, year;

		// the poker provider
		int provider;

		// max. number of seats
		int maxseats;

		// name of table
		std::wstring tablename;

		// the dealer sits at seat ...
		int buttonseat;

		// is a summary
		bool issummary;

		// the currency
		int currency;

		// times a showdown happened
		int showdowncounter;

		// was appended to container
		bool isappended;

		// the tourney identifier
		std::wstring tournament_name;

		// the tourney start date
		DateInfo tournament_startdate;

		// the tourney end date
		DateInfo tournament_enddate;

		// the tourney start time
		TimeInfo tournament_starttime;

		// the tourney end time
		TimeInfo tournament_endtime;

		// the tourney identifier
		int64 tournament_dbid;

		// the tourney buyin amount
		double tournament_buyin;

		// rebuy addon
		double tournament_rebuy;

		// prizepool
		double tournament_prizepool;

		// the bounty 
		double tournament_bounty;

		// the tournament add-on
		double tournament_addon;

		// the tournament limit
		int tournament_limittype;

		// tourney rake
		double tournament_rake;
	};*/
}

#endif // STANDARDTYPES_H 
