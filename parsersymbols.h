﻿#ifndef PARSERSYMBOLS_H
#define PARSERSYMBOLS_H

#include <boost/spirit/include/qi.hpp>
//#include "../pokerreadercommon/helper/definitions.h"



//typedef boost::spirit::real_parser<double, ts_real_policies<double> > ts_double;
//ts_ts_realtype const ts_double = ts_real_policies();

/*
///////////////////////////////////////////////////////////////////////////////
// Thousand parser sep: "," or "." e.g 100.000 or 100,000
///////////////////////////////////////////////////////////////////////////////
template <typename T>
struct ts_real_policies : boost::spirit::qi::ureal_policies<T>
{
	//  2 decimal places Max
	template <typename Iterator, typename Attribute>
	static bool parse_frac_n(Iterator& first, Iterator const& last, Attribute& attr)
	{
		return boost::spirit::qi::
			extract_uint<T, 10, 1, 3, true>::call(first, last, attr);
	}

	//  No exponent
	template <typename Iterator>
	static bool parse_exp(Iterator&, Iterator const&)
	{
		return false;
	}

	//  No exponent
	template <typename Iterator, typename Attribute>
	static bool parse_exp_n(Iterator&, Iterator const&, Attribute&)
	{
		return false;
	}
	template <typename Iterator>
	static bool parse_dot(Iterator& first, Iterator const& last)
	{
		if(first != last)
		{
			if( ((*first == '.') || (*first == ',')))
			{
				++first;
				return true;
			}
			return false;
		}
		return false;
	}

	//  Thousands separated numbers
	template <typename Iterator, typename Attribute>
	static bool parse_n(Iterator& first, Iterator const& last, Attribute& attr)
	{
		using boost::spirit::qi::uint_parser;
		namespace qi = boost::spirit::qi;

		uint_parser<unsigned, 10, 1, 3> uint3;
		uint_parser<unsigned, 10, 3, 3> uint3_3;
		uint_parser<unsigned, 10, 4, 20> uint4;

		bool commaasthit = false;
		bool pointasthit = false;
		bool commahdhit = false;
		bool resulthit = true;
		T n;
		Iterator save;

		T result = 0;
		if (parse(first, last, uint4, result))
		{
			resulthit = true;
			save = first;
		}
		else if (parse(first, last, uint3, result))
		{
			save = first;
			while (qi::parse(first, last, ',') && qi::parse(first, last, uint3_3, n))
			{
				result = result * 1000 + n;
				save = first;
				commaasthit = true;
				resulthit = commaasthit;
			}
			if(!commaasthit)
			{
				while (qi::parse(first, last, '.') && qi::parse(first, last, uint3_3, n))
				{
					result = result * 1000 + n;
					save = first;
					pointasthit = true;
					resulthit = pointasthit;
				}
			}
		}
		else
			return boost::spirit::qi::extract_uint<T, 10, 1, -1>::call(first, last, attr);
		first = save;
		if (resulthit)
			attr = result;
		return resulthit;
	}
};*/
///////////////////////////////////////////////////////////////////////////////
//  These policies can be used to parse thousand separated
//  numbers with at most 2 decimal digits after the decimal
//  point. e.g. 123,456,789.01
///////////////////////////////////////////////////////////////////////////////
/*
template <typename T>
struct ts_real_policies : boost::spirit::qi::ureal_policies<T>
{
	//  2 decimal places Max
	template <typename Iterator, typename Attribute>
	static bool
		parse_frac_n(Iterator& first, Iterator const& last, Attribute& attr)
	{
		return boost::spirit::qi::
			extract_uint<T, 10, 1, 2, true>::call(first, last, attr);
	}

	//  No exponent
	template <typename Iterator>
	static bool
		parse_exp(Iterator&, Iterator const&)
	{
		return false;
	}

	//  No exponent
	template <typename Iterator, typename Attribute>
	static bool
		parse_exp_n(Iterator&, Iterator const&, Attribute&)
	{
		return false;
	}

	//  Thousands separated numbers
	template <typename Iterator, typename Attribute>
	static bool
		parse_n(Iterator& first, Iterator const& last, Attribute& attr)
	{
		using boost::spirit::qi::uint_parser;
		namespace qi = boost::spirit::qi;

		uint_parser<unsigned, 10, 1, 3> uint3;
		uint_parser<unsigned, 10, 3, 3> uint3_3;

		T result = 0;
		if (parse(first, last, uint3, result))
		{
			bool hit = false;
			T n;
			Iterator save = first;

			while (qi::parse(first, last, ',') && qi::parse(first, last, uint3_3, n))
			{
				result = result * 1000 + n;
				save = first;
				hit = true;
			}

			first = save;
			if (hit)
				attr = result;
			return hit;
		}
		return false;
	}
};

namespace ParserSymbols
{
	namespace qi = boost::spirit::qi;
	namespace standard_wide = boost::spirit::standard_wide;

	struct LimitTypes_ : qi::symbols<wchar_t, unsigned>
	{
		LimitTypes_()
		{
			add
				(L"no limit hold'em"	, 1)
				(L"limit hold'em"	, 2)
				(L"pot limit hold'em", 0)

				// POKERSTARS
				(L"hold'em no limit"	, 1)
				(L"hold'em limit"	, 2)
				(L"hold'em pot limit", 0)
				;
		}

	} ;

	struct WithTypes_ : qi::symbols<wchar_t, unsigned>
	{
		WithTypes_()
		{
			add
				(L"with", 0)
				;
		}

	} ;

	struct IsFreeroll_ : qi::symbols<wchar_t, unsigned>
	{
		IsFreeroll_()
		{
			add
				(L"freeroll"	, 1)

				// POKERSTARS
				(L"freeroll"	, 1)
				;
		}

	} ;

	struct Bom_ : qi::symbols<wchar_t, unsigned>
	{

		Bom_()
		{
			wchar_t const* w1 = L"\ufeff";
			wchar_t const* w2 = L"\ufffe";
			add(w1);
			add(w2);
		}

	} ;

	struct StateTypes_ : qi::symbols<wchar_t, unsigned>
	{
		StateTypes_()
		{
			add
				(L"flop"	, FLOP)
				(L"turn"	, TURN)
				(L"river", RIVER)
				(L"show down", SHOWDOWN)

				// POKERSTARS
				(L"flop"	, FLOP)
				(L"turn"	, TURN)
				(L"river", RIVER)
				(L"show down", SHOWDOWN)
				;
		}

	} ;

	struct SeatTypes_ : qi::symbols<wchar_t, unsigned>
	{
		SeatTypes_()
		{
			add
				(L"seat"	, 0)

				// POKERSTARS
				(L"seat"	, TO)
				(L" in chips", TO)
				(L"is sitting out", TO)
				;
		}

	} ;



	struct PositionTypes_ : qi::symbols<wchar_t, unsigned>
	{
		PositionTypes_()
		{
			add
				(L"(small blind)", SB)
				(L"(big blind)", BB)
				(L"(button)", BU)

				// POKERSTARS
				(L"(small blind)", SB)
				(L"(big blind)", BB)
				(L"(button)", BU)
				;
		}

	} ;*/




	/*
	struct MonthTypes_ : qi::symbols<wchar_t, unsigned>
		{
			MonthTypes_()
			{
				add
					(L"January", 1)
					(L"February", 2)
					(L"March", 3)
					(L"April", 4)
					(L"May", 5)
					(L"June", 6)
					(L"July", 7)
					(L"August", 8)
					(L"September", 9)
					(L"October", 10)
					(L"November", 11)
					(L"December", 12)

					// POKERSTARS
					(L"January"	, 1)
					(L"February"	, 2)
					(L"March", 3)
					(L"April", 4)
					(L"May", 5)
					(L"June", 6)
					(L"July", 7)
					(L"August", 8)
					(L"September", 9)
					(L"October", 10)
					(L"November", 11)
					(L"December", 12)
					;
			}

		} ;

		struct ProviderTypes_ : qi::symbols<wchar_t, unsigned>
		{
			ProviderTypes_()
			{
				add
					(L"full tilt poker", Providers::FULLTILT)

					// POKERSTARS
					(L"pokerstars", POKERSTARS)
					;
			}

		} ;

		struct SummaryTypes_ : qi::symbols<wchar_t, unsigned>
		{
			SummaryTypes_()
			{
				add
					(L"tournament summary", TOURNAMENTSUMMARYFT)
					;
			}

		} ;

		struct ArticleTypes_ : qi::symbols<wchar_t, unsigned>
		{
			ArticleTypes_()
			{
				add
					(L"the", 0)
					;
			}

		} ;

		struct LanguageType_ : qi::symbols<wchar_t, unsigned>
		{
			LanguageType_()
			{
				add
					(L"game", ENGLISH)

					// POKERSTARS
					(L"game", ENGLISH)
					;
			}

		} ;

		struct IsTourney_ : qi::symbols<wchar_t, bool>
		{
			IsTourney_()
			{
				add
					(L"tournament", true)

					// POKERSTARS
					(L"tournament", true)
					;
			}

		} ;

		struct ShowActionType_ : qi::symbols<wchar_t, unsigned>
		{
			ShowActionType_()
			{
				add
					(L"shows", SHOWS)
					;
			}
		};

		struct CallBetBlindActionType_ : qi::symbols<wchar_t, unsigned>
		{
			CallBetBlindActionType_()
			{
				add
					(L"calls", CALL)
					(L"bets", BET)
					(L"folds", FOLD)
					(L"posts the small blind of", SMALLBLIND)
					(L"posts a dead small blind of", DEADSMALLBLIND)
					(L"posts the big blind of", BIGBLIND)
					(L"posts", DEADBIGBLIND)
					(L"antes", ANTE)

					// POKERSTARS
					(L"small+big blinds", SMALLANDBIGBLIND)
					(L"small+big blind", SMALLANDBIGBLIND)
					(L"small + big blinds", SMALLANDBIGBLIND)
					(L"small + big blind", SMALLANDBIGBLIND)
					(L"small+big", SMALLANDBIGBLIND)
					(L"small + big", SMALLANDBIGBLIND)
					(L"posts small&big blinds", SMALLANDBIGBLIND)
					(L"posts small&big blind", SMALLANDBIGBLIND)
					(L"posts small & big blinds", SMALLANDBIGBLIND)
					(L"posts small & big blind", SMALLANDBIGBLIND)
					(L"posts small&big", SMALLANDBIGBLIND)
					(L"posts small&big", SMALLANDBIGBLIND)
					(L"posts small blind", SMALLBLIND)
					(L"posts big blind", BIGBLIND)
					(L"posts the ante", ANTE)
					;
			}
		};

		struct UncalledBetActionType_ : qi::symbols<wchar_t, unsigned>
		{
			UncalledBetActionType_()
			{
				add
					(L"uncalled bet of", UNCALLEDBET)

					// POKERSTARS
					(L"uncalled bet", UNCALLEDBET)
					;
			}
		};

		struct RaiseActionType_ : qi::symbols<wchar_t, unsigned>
		{
			RaiseActionType_()
			{
				add
					(L"raises", RAISE)
					;
			}
		};

		struct FoldCheckMuckActionType_ : qi::symbols<wchar_t, unsigned>
		{
			FoldCheckMuckActionType_()
			{
				add
					(L"checks", CHECK)
					(L"folds", FOLD)
					(L"mucks", MUCK)
					;
			}
		};

		struct MuckedCardAction_ : qi::symbols<wchar_t, unsigned>
		{
			MuckedCardAction_()
			{
				add
					(L"mucked", MUCK)
					;
			}
		};

		struct CollectActionType_ : qi::symbols<wchar_t, unsigned>
		{
			CollectActionType_()
			{
				add
					(L"wins", COLLECT)
					(L"ties for", COLLECT)

					// POKERSTARS
					(L"collects", COLLECT)
					(L"collected", COLLECT)
					(L" finished the tournament in ", TOURNEYRESULT)
					;
			}
		};

		struct ActionType_ : qi::symbols<wchar_t, unsigned>
		{
			ActionType_()
			{
				add
				(L"checks", CHECK)
				(L"calls", CALL)
				(L"bets", BET)
				(L"folds", FOLD)
				(L"raises", RAISE)
				(L"posts the small blind of", SMALLBLIND)
				(L"posts a dead small blind of", DEADSMALLBLIND)
				(L"posts the big blind of", BIGBLIND)
				(L"posts", DEADBIGBLIND)
				(L"mucks", MUCK)
				(L"mucked", MUCK)
				(L"wins", COLLECT)
				(L"ties for", COLLECT)
				(L"uncalled bet", UNCALLEDBET)
				(L"antes", ANTE)
				(L"shows", SHOWS)
					;
			}

		} ;

		struct IsAllin_ : qi::symbols<wchar_t, unsigned>
		{
			IsAllin_()
			{
				add
					(L", and is all in", 1)

					// POKERSTARS
					(L"and is all-in", 1)
					;
			}

		} ;

		struct TourneyWin_ : qi::symbols<wchar_t, bool>
		{
			TourneyWin_()
			{
				add
					// POKERSTARS
					(L" and received ", true)
					;
			}

		};

		struct RakeType_ : qi::symbols<wchar_t, unsigned>
		{
			RakeType_()
			{
				add
					(L"rake", 0)
					;
			}

		};

		struct CurrencyTypes_ : qi::symbols<wchar_t, unsigned>
		{
			CurrencyTypes_()
			{
				add
					(L"$", USD)
					(L"€", EUR)
					(L"â¬", EUR)
					(L"FPP", FPP)
					;
			}
		};

		struct DealActionTypes_ : qi::symbols<wchar_t, unsigned>
		{
			DealActionTypes_()
			{
				add
					(L"dealt to", DEALEDCARDS)
					;
			}
		} ;

		struct CurrencyTypeLong_ : qi::symbols<wchar_t, unsigned>
		{
			CurrencyTypeLong_()
			{
				add
					(L"USD", USD)
					(L"EUR", EUR)
					;
			}
		} ;

		struct KeyWordTypes_ : qi::symbols<wchar_t, unsigned>
		{
			KeyWordTypes_()
			{
				add
					(L"to", TO)
					(L"$", 0)
					(L"from ", 0)
					//(L"from side pot", FROMPOT)
					//(L"from main pot", FROMPOT)
					(L"/", 0)
					(L"and won", WINS)
					(L"with", WINS)
					(L"returned to", TO)
					;
			}
		} ;

		struct ReturnedTypes_ : qi::symbols<wchar_t, unsigned>
		{
			ReturnedTypes_()
			{
				add
					(L"returned to", 0)
					;
			}
		};

		struct AnteTypes_ : qi::symbols<wchar_t, bool>
		{
			AnteTypes_()
			{
				add
					(L"ante", true)
					;
			}
		};


		struct ToTypes_ : qi::symbols<wchar_t, unsigned>
		{
			ToTypes_()
			{
				add
					(L"to", TO)
					;
			}
		} ;

		struct PotTypes_ : qi::symbols<wchar_t, unsigned>
		{
			PotTypes_()
			{
				add
					(L"pot", MAINPOT)
					(L"total pot", TOTALPOT)
					(L"main pot", MAINPOT)
					(L"side pot", SIDEPOT)

					// POKERSTARS
					(L"side pot-", SIDEPOT)
					;
			}

		} ;

		struct DealerSeatTypes_ : qi::symbols<wchar_t, unsigned>
		{
			DealerSeatTypes_()
			{
				add
					(L"the button is in seat #"	, 0)
					;
			}

		};

		struct GameStateTypes_ : qi::symbols<wchar_t, unsigned>
		{
			GameStateTypes_()
			{
				add
					(L"*** FLOP ***"	, FLOP)
					(L"*** TURN ***"	, TURN)
					(L"*** RIVER ***"	, RIVER)
					(L"*** SHOWDOWN ***", SHOWDOWN)
					;
			}
		};

		struct SitOutActionTypes_ : qi::symbols<wchar_t, unsigned>
		{
			SitOutActionTypes_()
			{
				add
					(L", is sitting out"	, FLOP)
					;
			}
		};

		struct TourneyPlayerActions_ : qi::symbols<wchar_t, unsigned>
		{
			TourneyPlayerActions_()
			{
				add
					(L"received"	, RECEIVED)
					(L"performed"	, PERFORMED)
					;
			}
		};

		struct TourneyKnockoutBounty_ : qi::symbols<wchar_t, unsigned>
		{
			TourneyKnockoutBounty_()
			{
				add
					(L"knockout bounty award"	, KNOCKOUTBOUNTY)
					(L"knockout bounty"	, KNOCKOUTBOUNTY)
					;
			}
		};

		struct TourneyPrizePool_ : qi::symbols<wchar_t, unsigned>
		{
			TourneyPrizePool_()
			{
				add
					(L"total prize pool"	, 0)
					;
			}
		};

		struct TourneyAddon_ : qi::symbols<wchar_t, unsigned>
		{
			TourneyAddon_()
			{
				add
					(L"add-on"	, ADDON)
					(L"add-ons"	, ADDON)
					;
			}
		};

		struct TourneyRebuy_ : qi::symbols<wchar_t, unsigned>
		{
			TourneyRebuy_()
			{
				add
					(L"rebuys"	, REBUY)
					(L"rebuy"	, REBUY)
					;
			}
		};

		struct TourneyBuyIn_ : qi::symbols<wchar_t, unsigned>
		{
			TourneyBuyIn_()
			{
				add
					(L"buy-in"	, 0)
					;
			}
		};

		struct TourneyStartTime_ : qi::symbols<wchar_t, unsigned>
		{
			TourneyStartTime_()
			{
				add
					(L"tournament started"	, 0)
					;
			}
		};

		struct TourneyEntries_ : qi::symbols<wchar_t, unsigned>
		{
			TourneyEntries_()
			{
				add
					(L"entries"	, 0)
					;
			}
		};

		static LimitTypes_ LimitType;
		static PotTypes_ PotType;
		static KeyWordTypes_ KeyWordType;
		static CurrencyTypeLong_ CurrencyTypeLong;
		static CurrencyTypes_ CurrencyType;
		static IsAllin_ IsAllin;
		static ActionType_ ActionType;
		static IsTourney_ IsTourney;
		static LanguageType_ LangType;
		static ProviderTypes_ ProviderType;
		static MonthTypes_ MonthType;
		static PositionTypes_ PositionTypes;
		static SeatTypes_ SeatTypes;
		static StateTypes_ StateType;
		static IsFreeroll_ Freeroll;
		static TourneyWin_ TourneyWin;
		static Bom_ BOM;
		static DealerSeatTypes_ DealerSeat;
		static ArticleTypes_ ArticleTypes;
		static WithTypes_ WithTypes;
		static ToTypes_ ToTypes;
		static ShowActionType_ ShowAction;
		static CollectActionType_ CollectAction;
		static FoldCheckMuckActionType_ FoldCheckMuckAction;
		static RaiseActionType_ RaiseAction;
		static CallBetBlindActionType_ CallBetBlindAction;
		static UncalledBetActionType_ UncalledBetAction;
		static ReturnedTypes_ ReturnType;
		static MuckedCardAction_ MuckCardAction;
		static RakeType_ RakeType;
		static DealActionTypes_ DealAction;
		static SitOutActionTypes_ SitOutAction;
		static GameStateTypes_ GameStates;
		static AnteTypes_ AnteType;
		static SummaryTypes_ SummaryType;
		static TourneyKnockoutBounty_ TourneyKnockoutBounty;
		static TourneyPlayerActions_ TourneyPlayerAction;
		static TourneyAddon_ TourneyAddon;
		static TourneyRebuy_ TourneyRebuy;
		static TourneyBuyIn_ TourneyBuyIn;
		static TourneyPrizePool_ TourneyPrizePool;
		static TourneyStartTime_ TourneyStartTime;
		static TourneyEntries_ TourneyEntries;*/
namespace ParserSymbols
{
	struct Players : boost::spirit::qi::symbols<wchar_t, std::wstring>
	{
		Players()
		{
			add(L"split pot", L"split pot");
		}
	};

	static Players PLAYERNAMES;
	static Players OWNPLAYERNAME;

	static void addplayername_asstring(std::wstring name)
	{
		if(PLAYERNAMES.find(name) == NULL)
		{
			PLAYERNAMES.add(name, name);
		}
	}

	static void addplayername_asvector(std::vector<wchar_t> namevector)
	{
		std::wstring playername(namevector.begin(), namevector.end());
		addplayername_asstring(playername);
	}

	static void addownplayername_asstring(std::wstring name)
	{
		if(OWNPLAYERNAME.find(name) == NULL)
		{
			OWNPLAYERNAME.add(name, name);
		}
	}

	static void addownplayername_asvector(std::vector<wchar_t> namevector)
	{
		std::wstring playername(namevector.begin(), namevector.end());
		addplayername_asstring(playername);
	}
}

#endif // PARSERSYMBOLS_H
